package com.testsg.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

public class IOUtils {
	public final static Logger logger = Logger.getLogger(IOUtils.class);

	public static final class OsUtils {

		private static String OS = null;

		public static String getOsName() {
			if (OS == null) {
				OS = System.getProperty("os.name");
			}
			return OS;
		}

		public static boolean isWindows() {
			return getOsName().startsWith("Windows");
		}

		public static boolean isMac() {
			return getOsName().startsWith("Mac");
		}
	}

	public static void createFileIfDoesNotExists(InputStream in, String path) {
		FileOutputStream out=null;

		try {
			out = new FileOutputStream(path);
			byte[] buffer = new byte[1024];
			int len = in.read(buffer);
			while (len != -1) {
				out.write(buffer, 0, len);
				len = in.read(buffer);
			}
		} catch (FileNotFoundException e) {
			logger.debug("FileNotFoundException  - ",e);
		} catch (IOException e) {
			logger.debug("IOException  - ",e);
		}finally{

			try {
				out.close();
			} catch (IOException e) {
				logger.debug("IOException  - ",e);
			}

		}

	}

}
