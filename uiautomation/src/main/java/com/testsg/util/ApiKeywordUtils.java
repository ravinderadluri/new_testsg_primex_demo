package com.testsg.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.testsg.test.InputDataKeywords;
import com.testsg.test.OutputDataKeywords;

/// The purpose of this class is to call the methods.
/// It is not used to store data
public final class ApiKeywordUtils {
	public final static Logger logger = Logger.getLogger(IOUtils.class);
	public static long numberOfAsserts; 
	
	public static Method inputDataMethods[];
	public static Method outputDataMethods[];
	public static InputDataKeywords inputDataKeywords;
	public static OutputDataKeywords outputDataKeywords;
	
	public static String runInputDataKeyword(String currentKeyword)
	{
		return runKeyword(currentKeyword, inputDataMethods, inputDataKeywords);
	}
	
	public static String runOutputDataKeyword(String currentKeyword)
	{
		return runKeyword(currentKeyword, outputDataMethods, outputDataKeywords);
	}
	
	public static boolean isOutputDataKeyword(String currentKeyword)
	{
		if (currentKeyword.trim().isEmpty())
			return false;
		else if(getKeywordIndex(currentKeyword, outputDataMethods) != -1)
			return true;
		return false; 
	}
	
	public static boolean isInputDataKeyword(String currentKeyword)
	{
		if (currentKeyword.trim().isEmpty())
			return false;
		else if(getKeywordIndex(currentKeyword, inputDataMethods) != -1)
			return true;
		return false; 
	}
	
	public static int getKeywordIndex(String currentKeyword, Method[] mArr) 
	{
		return Arrays.stream(mArr).map(s->s.getName()).collect(Collectors.toList()).indexOf(currentKeyword);
	}
	
	private static String runKeyword(String currentKeyword, Method[] mArr, Object objInstance )
	{
		String keyword_execution_result = "";
		logger.debug("Checking index where KEYWORD = " + currentKeyword);
		int index = getKeywordIndex(currentKeyword, mArr);	
		try {
			keyword_execution_result = (String) mArr[index].invoke(objInstance);
		} catch (IllegalAccessException e) {
			logger.debug("Run Keyword [" + currentKeyword + "] Failed");
			numberOfAsserts++;
			e.getCause().printStackTrace();
		} catch (IllegalArgumentException e) {
			logger.debug("Run Keyword [" + currentKeyword + "] Failed");
			numberOfAsserts++;
			e.getCause().printStackTrace();
		} catch (InvocationTargetException e) {
			logger.debug("Run Keyword [" + currentKeyword + "] Failed");
			numberOfAsserts++;
			e.getCause().printStackTrace();
		}
		return keyword_execution_result;
	}


}
