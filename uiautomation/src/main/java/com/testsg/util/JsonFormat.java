package com.testsg.util;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;


/// Utility Class to use a Xpath like format to traverse the embedded JSON tree to find a value or set a value
public final class JsonFormat {

	// Convert File or JSON payload String into a JsonNode type
	private static JsonNode toJsonNode(String payload) {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsNode = null;
		try {
			if(payload.contains("{"))
				jsNode = mapper.readTree(payload);
			else
				jsNode = mapper.readTree(new File(payload));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsNode;
	}
	
	private static JsonNode findJsonNodeFromKey(String key, JsonNode payload) {
		int arrayEnd = 0;
		JsonNode js = payload;
		// find the value where the key looks like "response.results.workListView.filters[0].link.rel" or "workListView"
		if(key.contains(".")) {
			String[] series = key.split(Pattern.quote("."));
			for(String str : series) {
				if(str.contains("[")) {
					arrayEnd = Integer.parseInt(str.replaceAll("\\D+",""));
					str = str.substring(0, str.indexOf("["));
					js = js.path(str).path(arrayEnd);
				}
				else
					js = js.path(str);
				
				if (js.isNull()) {
					break;
				}
				else if (js.isArray() && (arrayEnd < js.size())) {
					Iterator<JsonNode> iterator = js.elements();
					for ( int i = 0; (iterator.hasNext() && i <= arrayEnd); i++ )
						js = iterator.next();
				}
	    	}
		}
		else
			js = payload.findPath(key);
		return js;
	}

	private static String findJsonValue(String key, JsonNode payload) {
		String returnValue = null;
		JsonNode js = findJsonNodeFromKey(key, payload);

		if (!js.isNull()) {
			returnValue = js.toString();
			// JSON data sometimes comes with extra set of double-quotes, remove them
			if (returnValue.indexOf("\"") == 0 && returnValue.lastIndexOf("\"")+1 == returnValue.length())
				returnValue = returnValue.substring(returnValue.indexOf("\"")+1, returnValue.lastIndexOf("\""));
		}
		return returnValue;
	}
	
	/**
	 * @param key
	 * @param value
	 * @return New Payload : if key was found, and value was set and return a new payload
	 * if key not found, "" blank payload is returned
	 */
	public static String setJsonValueToPayload(String key, String value, String jsonPayload) {
		JsonNode original = toJsonNode(jsonPayload);
		JsonNode js = original;
		String lastkey = key.substring(key.lastIndexOf(".")+1);
		String parentKey = key.substring(0, key.lastIndexOf("."));
		if (!js.isNull()) {
			js = findJsonNodeFromKey(parentKey, js);
			if(!js.isNull()) {
				((ObjectNode)js).put(lastkey, value);
			}
			else return "";
		}
		else return "";
		return original.toString();
	}
	
	/**
	 * @param key
	 * @param FileNameORpayload
	 * @return Complete JSON payload value that is from String payload or full file path/fileName of JSON
	 */
	public static String getJsonFromKeyAndFilename(String key, String FileNameORpayload) {
		return getValueFromKeyAndJsonPayload(key, FileNameORpayload);
	}
	
	
	/**
	 * @param key | can look like "response.results.workListView.filters[2].link"
	 * @return String value of internally stored JSON payload. Can be the String value of one item or the entire nested String payload
	 */
	public static String getValueFromKeyAndJsonPayload(String key, String payload) {
		JsonNode jsonPayload = toJsonNode(payload);
		return findJsonValue(key, jsonPayload);
	}
}
