package com.testsg.test;

import static com.testsg.test.DriverScript.configProperties;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import autoitx4java.AutoItX;

public class DesktopKeywords {

	Screen screen = new Screen();
	String root = System.getProperty("user.dir");
	String sikuroot = root + File.separator + "SikuliImageObj" + File.separator;
	final Logger logger = Logger.getLogger(DesktopKeywords.class);
	String OrderNo;
	public static WebDriver driver;
	
	AutoItX AutoIt = new AutoItX();
	
	public ExtentTest extentTest;
 	public KeywordData keywordData = new KeywordData();

 
	// Captures the screen shot and saves in provided filename location
	public String captureScreenshot(String screenShotName, WebDriver driver) throws IOException {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String dest = System.getProperty("user.dir") + "\\Screenshots\\" + screenShotName + ".png";
		File destination = new File(dest);
		FileUtils.copyFile(source, destination);
		return dest;
	}

	public String testsgDate() {
		Date d = new Date();
		String date = d.toString().replaceAll(" ", "_");
		date = date.replaceAll(":", "_");
		date = date.replaceAll("\\+", "_");
		return date;
	}

	// Captures the screen shot and saves in provided filename location
	public void captureScreenshot(String filename, String keyword_execution_result, String screenShotOnDemand)
			throws IOException {
		// take screen shots

		if (configProperties.getProperty("testLocation").equals("Local")) {
		}
		if (keyword_execution_result.startsWith(Constants.KEYWORD_FAIL)) {
			// capture screenshot
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(filename + ".png"));
		} else if (keywordData.screenShotOnDemand.equals("Y")) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(filename + ".png"));
		}
	}

	/************************
	 * Windows specific Keywords - AutoIt Methods
	 ********************************/

	// Runs the file using the file location provided in the data
	public String autoItRun() {

		logger.debug("Running " + "'" + keywordData.data + "'");
		try {

			AutoIt.run(keywordData.data);

		} catch (Exception e) {
			extentTest.log(LogStatus.FAIL,
					"Failed to run" + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Not able to run autoIT command";

		}
		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "Ran Auto IT command" + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "Run auto IT - Failed");
		}

		return Constants.KEYWORD_PASS;
	}

	// Closes the application
	public String autoItClose() {

		logger.debug("Closing " + "'" + keywordData.data + "'");
		try {
			AutoIt.winClose(keywordData.data);

		} catch (Exception e) {
			extentTest.log(LogStatus.FAIL,
					"Failed to run auto IT close  " + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + "Unable to close";
		}
		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, " closed sucessfully", keywordData.screenShotPath + ".png");
		} else
			extentTest.log(LogStatus.PASS, " closed sucessfully");

		return Constants.KEYWORD_PASS;
	}

	public String autoItActivate() {

		logger.debug("'" + keywordData.data + "'" + " activated");
		try {

			AutoIt.winActivate(keywordData.data);
			AutoIt.winWaitActive(keywordData.data);

		} catch (Exception e) {
			extentTest.log(LogStatus.FAIL, "Failed to run auto IT activate  "
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Not able to run autoIT command auto IT Activate";
		}
		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "Ran Auto IT command" + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "Run auto IT Actvated - Failed");
		}
		return Constants.KEYWORD_PASS;
	}

	public String autoItCtrlSetText() {
		logger.debug("Entering text " + "'" + keywordData.data + "'");
		try {
			// object = keyValues.xmlp(locator);
			String title = keywordData.locator.split(Constants.DATA_SPLIT)[0];
			String text = keywordData.locator.split(Constants.DATA_SPLIT)[1];
			String control = keywordData.locator.split(Constants.DATA_SPLIT)[2];
			String string = keywordData.data;
			AutoIt.winWait(title, text, 5);
			AutoIt.ControlSetText(title, text, control, string);

		} catch (Exception e) {
			extentTest.log(LogStatus.FAIL, "Failed to run control set text "
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Not able to run autoIT command control set text";
		}
		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS,
					"Ran Auto IT command" + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.FAIL, "Run auto IT control set text Failed");
		}
		return Constants.KEYWORD_PASS;
	}

	public String autoItSend() {

		logger.debug("Sending keyboard imput " + "'" + keywordData.data + "'");
		try {

			AutoIt.send(keywordData.data);

		} catch (Exception e) {
			extentTest.log(LogStatus.FAIL,
					"Failed to run  auto IT send " + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Not able to run autoIT command auto IT send";

		}
		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "Ran Auto IT send command" + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "Run auto IT send - Failed");
		}

		return Constants.KEYWORD_PASS;
	}

	/************************
	 * Windows specific Keywords - AutoIt Methods sdriver is a SikuliFirefoxDriver
	 * to drive interactions with a web page using both image recognition (Sikuli)
	 * and DOM (Selenium).
	 * 
	 ********************************/
	public String sikuScrnRightClick() throws FindFailed, InterruptedException {
		logger.debug("Right clicking on button ");

		try {
			keywordData.locator = sikuroot + keywordData.locator;
			screen.find(keywordData.locator);
			screen.rightClick(keywordData.locator);

		} catch (Exception e) {
			extentTest.log(LogStatus.FAIL, "Failed to run  sikuli right click"
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " -- Not able to right click button" + e.getMessage();

		}
		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "Right Clicked Successfully" + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.FAIL, "Right Click wit autoIT Failed");
		}
		return Constants.KEYWORD_PASS;

	}

	public String sikuScrnClick() throws FindFailed, InterruptedException {
		logger.debug("Clicking on button ");
		try {
			keywordData.locator = sikuroot + keywordData.locator;
			screen.find(keywordData.locator);
			screen.click(keywordData.locator);

		} catch (Exception e) {
			extentTest.log(LogStatus.FAIL, "Failed to run  sikuli screen click"	+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " -- Not able to click button with sikuli" + e.getMessage();

		}
		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "sikuli Clicked Successfully" + keywordData.ObjectName	+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "sikuli Click successfull");
		}

		return Constants.KEYWORD_PASS;
	}

}