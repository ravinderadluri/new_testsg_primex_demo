package com.testsg.test;

import org.apache.log4j.Logger;

import com.cedarsoftware.util.io.JsonWriter;
import com.relevantcodes.extentreports.LogStatus;
import com.testsg.util.JsonFormat;

public class OutputDataKeywords {
	final Logger logger = Logger.getLogger(OutputDataKeywords.class);
	public RestApiData data = new RestApiData();

	public String Payload() {
		data.xlsColNum++;
		data.extentTest.log(LogStatus.WARNING, "Payload() NOT implemented yet");
		return Constants.KEYWORD_SKIP + " - Payload() Not implemented yet";
	}

	// The name of the variable will be first searched.  If not found inside, then the contents will be replaced by the variable's value
	public String Set() {
		String key = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		String value = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		String payloadValue = "";
		// Expected variable name cell cannot be empty, but the next cell can should the user want to clear it out
		if (key.isEmpty()) {
			data.extentTest.log(LogStatus.FAIL, "Excel cell after SET is empty");
			return Constants.KEYWORD_FAIL + "Excel cell after SET is empty";
		}
		// check if data2 is a JSON branch by looking for periods
		if (value.contains(".")) { 
			payloadValue = JsonFormat.getValueFromKeyAndJsonPayload(value, data.responseJsonPayload);
			// Set the RestApiData's hashmap[data1] = payloadValue
			data.variables.put(key, payloadValue);
			data.extentTest.log(LogStatus.PASS, "data["+key+"] = "+payloadValue);
			return Constants.KEYWORD_PASS + " - Set: key = ["+key+"] | value = ["+payloadValue+"]";
		}
		else {
			data.variables.put(key, value);
			data.extentTest.log(LogStatus.PASS, "data["+key+"] = "+value);
		}
		return Constants.KEYWORD_PASS + " - Set: key = ["+key+"] | value = ["+value+"]";
	}

	
	/** Exist keyword verifies that the key-value pair is TRUE
	 *  First Cell is the key that represents a JSON/XML path to search in the payload or can be a key for the internally stored hashmap
	 *  Second Cell is the value to 
	 * @return
	 */
	public String Exist() {
		String key = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		String value = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		String storedValue = JsonFormat.getValueFromKeyAndJsonPayload(key, data.responseJsonPayload);
		String reportFmtStart = "<textarea rows=\"4\" cols=\"150\" style=\"background-color: #f3f3f3;\" readonly>";
		if (storedValue.equals(value)) {
			data.extentTest.log(LogStatus.PASS, "VerifyTrue: key = ["+key+"] | value = ["+value+"] was found inside the Json Payload = \n"+ reportFmtStart + JsonWriter.formatJson(data.responseJsonPayload)+"</textarea>");
			return Constants.KEYWORD_PASS + " - VerifyTrue: key = ["+key+"] | value = ["+value+"] was found within the JSON = \n" + data.responseJsonPayload;
		}
		data.extentTest.log(LogStatus.FAIL, "VerifyTrue: key = ["+key+"] | value = ["+value+"] was not found inside the Json Payload = \n"+ reportFmtStart + JsonWriter.formatJson(data.responseJsonPayload)+"</pre>");
		return Constants.KEYWORD_FAIL + " - VerifyTrue: key = ["+key+"] | value = ["+value+"] is NOT found inside the JSON = \n" + data.responseJsonPayload;
	}

	/**
	 * 
	 * @return
	 */
	public String NotExist() {
		String key = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		String value = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		String storedValue = JsonFormat.getValueFromKeyAndJsonPayload(key, data.responseJsonPayload);
		String reportFmtStart = "<textarea rows=\"4\" cols=\"150\" style=\"background-color: #f3f3f3;\" readonly>";
		if (!storedValue.equals(value)) {
			data.extentTest.log(LogStatus.PASS, "key = ["+key+"] | value = ["+value+"] was not found inside the Json Payload = \n"+ reportFmtStart + JsonWriter.formatJson(data.responseJsonPayload)+"</textarea>");
			return Constants.KEYWORD_PASS + " - key = ["+key+"] | value = ["+value+"] is doesn't exist inside the payload = \n" + data.responseJsonPayload;
		}
		data.extentTest.log(LogStatus.FAIL, "key = ["+key+"] | value = ["+value+"] was not found inside the Json Payload = \n"+ reportFmtStart + JsonWriter.formatJson(data.responseJsonPayload)+"</textarea>");
		return Constants.KEYWORD_FAIL + " - key = ["+key+"] | value = ["+value+"] is inside the payload = \n" + data.responseJsonPayload;
	}
}
