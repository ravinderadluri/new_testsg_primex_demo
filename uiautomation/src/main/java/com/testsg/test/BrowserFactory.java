package com.testsg.test;

import static com.testsg.util.IOUtils.OsUtils.isWindows;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class BrowserFactory {
	private static WebDriver driver;

	public  static WebDriver getRemoteDriver(KeywordData keywordData,File cfile,File driverformac) throws MalformedURLException{
		if (!keywordData.hubURL.isEmpty() && keywordData.browserType.equalsIgnoreCase("Firefox") ){
			driver = new RemoteWebDriver(new URL(keywordData.hubURL),DesiredCapabilities.firefox());
		} else if (!keywordData.hubURL.isEmpty() && keywordData.browserType.equalsIgnoreCase("chrome")){
			driver = new RemoteWebDriver(new URL(keywordData.hubURL), DesiredCapabilities.chrome());
		} else if (!keywordData.hubURL.isEmpty() && keywordData.browserType.equalsIgnoreCase("IE") ){
			driver = new RemoteWebDriver(new URL(keywordData.hubURL),  DesiredCapabilities.internetExplorer());
		} else if (keywordData.hubURL.isEmpty() && keywordData.browserType.equalsIgnoreCase("IE")) {
			driver = new InternetExplorerDriver();		
		} else if (keywordData.hubURL.isEmpty() && keywordData.browserType.equalsIgnoreCase("Chrome")) {
			if (isWindows()){
				System.setProperty("webdriver.chrome.driver", cfile.getAbsolutePath());
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--test-type");
				driver = new ChromeDriver(options);
			}else{
				System.setProperty("webdriver.chrome.driver", driverformac.getAbsolutePath());
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--test-type");
				driver = new ChromeDriver();
			}
		}
		else if(keywordData.hubURL.isEmpty() && keywordData.browserType.equalsIgnoreCase("Safari")){
			System.setProperty("webdriver.safari.noinstall", "true");
			driver=new SafariDriver();
		}	 else if(keywordData.hubURL.isEmpty() && keywordData.browserType.equalsIgnoreCase("phantom")) {
			driver = new PhantomJSDriver();
		}			
		else if(keywordData.hubURL.isEmpty() && keywordData.browserType.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
		}	
		return driver;
	}

}
