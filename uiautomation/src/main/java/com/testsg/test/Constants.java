package com.testsg.test;

public class Constants {

	public static final String TEST_SUITE_SHEET = "Test Suite";
	public static final String TEST_CASE_ID = "ACTION";
	public static final String DATA_SHEET="Input_Data";
	public static final String RUNMODE_YES = "Y";
	public static final String TEST_CASES_SHEET = "Test Cases";
	public static final String RUNMODE = "Runmode";
	public static final String TCID = "TCID";
	public static final String TEST_STEPS_SHEET = "Test Steps";
	public static final String KEYWORD = "Keyword";
	public static final String KEYWORD_PASS = "PASS";
	public static final String KEYWORD_FAIL = "FAIL";
	public static final String PROCEED_ON_FAIL = "Proceed_on_Fail";
	public static final String SCRNSHOTONDEMAND = "Screenshot? Y/N";
	public static final String OBJNAME="ObjectName";
	public static final String OUTPUT_DATA = "Output_Data";
	public static final String RESULT = "Result";
	public static final String KEYWORD_SKIP = "SKIP";
	public static final String DATA = "Input_Data";
	public static final String OBJECT = "Object";
	public static final String DATA_START_COL = "col";
	public static final String DATA_SPLIT = "\\|";
	public static final String DOCTITLE = "TestSG Automation Report";
	public static final String REPORTNAME = "AUTOMATION REPORT: ";
	public static final String REPORTHEADLINE = "Google Flight";
	public static final Object RANDOM_VALUE = "Random";
	public static final String REPORTS = "Reports";
	public static final String TOOLS = "tools";
	public static final String APK = "apk";
	public static final String SIKULIIMGOBJ= "SikuliImageObj";
	public static final String AUTOMATIONREPORT = "AutomationReport.html";
	public static final String CONFIGXML = "config.xml";
	public static final String ORXML = "or.xml";
	public static final String SUITEXlSX = "Suite.xlsx";
	public static final String XLS = "xls";
	public static final String USERDIR="user.dir";
	public static final String SCREENSHOTS="screenshots";
	public static final String CAPTURESCREENSHOT="captureScreenshot";
	public static final String CHROMEDRIVER="chromedriver.exe";
	public static final String CHROMEDRIVER_MAC="chromedriver_mac64";
	public static final String IEDRIVER="IEDriverServer.exe";
	public static final String FIREFOXDRIVER="geckodriver.exe"; 
	public static final String PHANTOMDRIVER="phantomjs.exe";
	public static final String INPUTFOLDER="/tests";
	public static final String CONFIG = "config";
	public static final String CONFIGFOLDER="/config";
	public static final String REPORTXML="extentconfig.xml";


}
