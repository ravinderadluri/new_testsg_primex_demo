package com.testsg.test;

/**
 * Data Structure purpose is to publicly share data between the DriverScript and Keywords methods
 * Will live inside the Keywords Class
 */
public class KeywordData {
	// Selenium Grid HUB URL
	public String hubURL;
	// Browser type name
	public String browserType;
	// Initial URL to begin test
	public String navURL;
	// WebElement XPath or CSS Selector
	public String locator;
	//WebElement object description
	public String ObjectName;
 	// Keyword method data
	public String data;
	// Screenshot capture path
	public String screenShotPath;
	// Y or N character representing to take a screenshot
	public String screenShotOnDemand;
	
	public String title;
	public String text;
	public String control;
 	
	public void clearBrowserInfo() {
		hubURL = "";
		browserType = "";
		navURL = "";
	}
	
	public void clearTestData() {
		ObjectName ="";
		locator = "";
		data = "";
		screenShotPath = "";
		screenShotOnDemand = "";
	}
	
	public void clearAll() {
		this.clearBrowserInfo();
		this.clearTestData();
	}
}
