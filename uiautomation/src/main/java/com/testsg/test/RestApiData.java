package com.testsg.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.relevantcodes.extentreports.ExtentTest;

public class RestApiData {
	// Cell data from DATA Excel Sheet to be managed by the RestApiKeywords method
	public String[] DataObjects = new String[50];
	// Set Temporary storage variables for payload manipulation
	public HashMap<String, String> variables = new HashMap<String, String>();
	// Flag to signal that keyword method has taken responsibility to perform the Post/Get method defined in under the METHOD Excel column.
	public boolean overrideMethod;
	public Xls_Reader xlsDataSheet;
	public String xlsSheetTabName;
	public int xlsRowNum;
	public int xlsColNum;
	// 2 Security tokens needed to perform Requests
	public String X_Agencyport_Token;
	public String X_Agencyport_CSRF_Token;
	// List of Headers to be used in request
	public Map<String, String> requestHeaders = new ConcurrentHashMap<String, String>();
	// Content-Type request
	public String requestContentType;
	// Saved Payload from GET Response to be prepared for POST request
	public String requestBody;
	// GET or POST request URL
	public String requestURL;
	// Saved Response payloads for reuse
	public String responseJsonPayload;
	public String responseXmlPayload;

	// Extent Test for reports
	public ExtentTest extentTest;

	public void clearAll() {
		Arrays.fill(DataObjects, "");
		variables.clear();
		requestHeaders.clear();
		requestContentType = "";
		requestBody = "";
		requestURL = "";
		X_Agencyport_Token = "";
		X_Agencyport_CSRF_Token = "";
		responseJsonPayload = "";
	}
}
