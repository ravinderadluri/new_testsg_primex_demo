package com.testsg.test;

import static com.testsg.test.DriverScript.configProperties;
import static com.testsg.util.IOUtils.OsUtils.isWindows;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import autoitx4java.AutoItX;

public class Keywords {

	Screen screen = new Screen();
	String root = System.getProperty("user.dir");
	String sikuroot = root + File.separator + "SikuliImageObj" + File.separator;
	final Logger logger = Logger.getLogger(Keywords.class);
	String OrderNo;
	public static WebDriver driver;
	
	AutoItX AutoIt = new AutoItX();
	
	public ExtentTest extentTest;
 	public KeywordData keywordData = new KeywordData();

 	public String openBrowser() throws MalformedURLException {
		try {
			File browserDriverPaths = new File(
					System.getProperty(Constants.USERDIR) + File.separator + Constants.TOOLS);
			if (!browserDriverPaths.exists()) {
				browserDriverPaths.mkdirs();
			}

			File cfile = new File(browserDriverPaths.getAbsolutePath(), Constants.CHROMEDRIVER);

			File driverformac = new File(browserDriverPaths.getAbsolutePath(), Constants.CHROMEDRIVER_MAC);

			File ffile = new File(browserDriverPaths.getAbsolutePath(), Constants.FIREFOXDRIVER);
			System.setProperty("webdriver.gecko.driver", ffile.getAbsolutePath());

			File pjsfile = new File(browserDriverPaths.getAbsolutePath(), Constants.PHANTOMDRIVER);
			System.setProperty("phantomjs.binary.path", pjsfile.getAbsolutePath());

			if (isWindows()) {
				File iefile = new File(browserDriverPaths.getAbsolutePath(), Constants.IEDRIVER);
				System.setProperty("webdriver.ie.driver", iefile.getAbsolutePath());
			}

			logger.info("Opening browser");

			System.out.println("Hub URL=" + keywordData.hubURL);
			System.out.println(keywordData.browserType);

			driver = BrowserFactory.getRemoteDriver(keywordData, cfile, driverformac);
			long implicitWaitTime = Long.parseLong(configProperties.getProperty("implicitwait"));
			driver.manage().timeouts().implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);

		} catch (NumberFormatException e) {
			logger.debug("NumberFormatException in openBrowser - ", e);
		}
		return Constants.KEYWORD_PASS;
	}

	// maximize the window
	public String maximizeWindow() {
		logger.info("Maximizig the window ");

		try {
			// driver.manage().window().maximize();
			driver.manage().window().setSize(new Dimension(1800, 860));
		} catch (Exception e) {
			logger.debug("Exception in maximizeWindow - ", e);

			extentTest.log(LogStatus.FAIL,
					"Not able to maximize window " + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " -- Not able to maximize window" + e.getMessage();
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "Window maximized sucessfully:" + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "Window maximized " + keywordData.ObjectName);
		}
		return Constants.KEYWORD_PASS;
	}

	// Navigates to website using the url provided through data
	public String navigate() throws IOException {
		logger.info("Navigating to " + "'" + keywordData.navURL + "'");
		try {
			driver.navigate().to(keywordData.navURL);

		} catch (Exception e) {
			logger.debug("Exception in navigate - ", e);
			extentTest.log(LogStatus.FAIL, "Not able to navigate to " + keywordData.ObjectName + keywordData.locator
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " -- Not able to navigate";
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "Navigated sucessfully");
		} else {
			extentTest.log(LogStatus.FAIL, "Navigation Failed");
		}

		return Constants.KEYWORD_PASS;
	}

	// Captures the screen shot and saves in provided filename location
	public String captureScreenshot(String screenShotName, WebDriver driver) throws IOException {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String dest = System.getProperty("user.dir") + "\\Screenshots\\" + screenShotName + ".png";
		File destination = new File(dest);
		FileUtils.copyFile(source, destination);
		return dest;
	}

	// Verifies page title with the expected title(provide through data)
	public String verifyTitle() {
		logger.info("Verifying title");
		try {
			String actualTitle = driver.getTitle();

			String expectedTitle = keywordData.data;
			if (actualTitle.equals(expectedTitle)) {
				if (keywordData.screenShotOnDemand.equals("Y")) {
					extentTest.log(LogStatus.PASS, "Title verified sucessfully"
							+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
				} else {
					extentTest.log(LogStatus.PASS, "Title verified sucessfully");
				}
				return Constants.KEYWORD_PASS + "verify title.........";
			} else {
				extentTest.log(LogStatus.FAIL, "Title not verified " + expectedTitle + " -- " + actualTitle);
				return Constants.KEYWORD_FAIL + " -- Title not verified " + expectedTitle + " -- " + actualTitle;
			}
		} catch (Exception e) {
			logger.debug("Exception in verifyTitle - ", e);
			extentTest.log(LogStatus.FAIL, "Error in retrieving title");
			return Constants.KEYWORD_FAIL + " Error in retrieving title";
		}
	}

	public String waitForElement() {
		logger.info("waiting for element to present");
		int dat = Integer.parseInt(keywordData.data);
		System.out.println(dat);
		WebDriverWait wait = new WebDriverWait(driver, dat);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(keywordData.locator)));
		} catch (Exception e) {
			logger.debug("Exception in writeInInputId - ", e);

			extentTest.log(LogStatus.FAIL, "Element is not visible " + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Element is not visible " + e.getMessage();
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "Element is visible" + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "Element is seen");
		}

		return Constants.KEYWORD_PASS;
	}

	public String writeInputByXpath() throws IOException {
		logger.info("Writing in text box");
		try {
			driver.findElement(By.xpath(keywordData.locator)).clear();
			driver.findElement(By.xpath(keywordData.locator)).sendKeys(keywordData.data);
		} catch (Exception e) {
			logger.debug("Exception in writeInInput - ", e);
			extentTest.log(LogStatus.FAIL, "Not able to write " + keywordData.locator
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Unable to write " + e.getMessage();
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
 			extentTest.log(LogStatus.PASS, "Entered input to " + keywordData.ObjectName + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "Entered input to " + keywordData.ObjectName);
		}

		return Constants.KEYWORD_PASS;

	}

	public String writeInputByID() throws IOException {
		logger.info("Writing in text box");
		try {
			driver.findElement(By.id(keywordData.locator)).clear();
			driver.findElement(By.id(keywordData.locator)).sendKeys(keywordData.data);
		} catch (Exception e) {
			logger.debug("Exception in writeInInput - ", e);
			extentTest.log(LogStatus.FAIL, "Not able to write " + keywordData.locator
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Unable to write " + e.getMessage();
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
			String abc = captureScreenshot("abc", driver);
			extentTest.log(LogStatus.PASS, "Entered input to " + keywordData.ObjectName,
					extentTest.addScreenCapture(abc));
		} else {
			extentTest.log(LogStatus.PASS, "Entered input to " + keywordData.ObjectName);
		}

		return Constants.KEYWORD_PASS;

	}

	public String clickByXpath() {
		logger.info("Clicking on " + keywordData.locator + "" + " " + keywordData.data + "");
		try {
			driver.findElement(By.xpath(keywordData.locator)).click();
		} catch (Exception e) {
			logger.debug("Exception in click - ", e);

			extentTest.log(LogStatus.FAIL, "Not able to click " + keywordData.locator
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Not able to click";
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "clicked on " + "  " + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "clicked on" + " " + keywordData.ObjectName);
		}
		return Constants.KEYWORD_PASS;
	}

	public String clickByID() {
		logger.info("Clicking on " + keywordData.locator + "" + " " + keywordData.data + "");
		try {
			driver.findElement(By.id(keywordData.locator)).click();
		} catch (Exception e) {
			logger.debug("Exception in click - ", e);

			extentTest.log(LogStatus.FAIL, "Not able to click " + keywordData.locator
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Not able to click";
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "clicked on " + "  " + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "clicked on" + " " + keywordData.ObjectName);
		}
		return Constants.KEYWORD_PASS;
	}

	public String clickFromDropdown() {
		try {

			List<WebElement> allElements = driver.findElements(By.xpath(keywordData.locator));

			for (WebElement element : allElements) {
				if (keywordData.data.equals(element.getText())) {
					element.click();
					break;
				}
			}
		}

		catch (Exception e) {
			logger.debug("Exception in selectListValue - ", e);
			extentTest.log(LogStatus.FAIL, "Could not select from list " + keywordData.ObjectName + keywordData.locator
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + "Failed to Select value";
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "Selected the Account successfully"
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "Selected Account from list");
		}
		return Constants.KEYWORD_PASS + "Successfully selected value";

	}

	public String selectFromDropdown() {
		logger.info("Selecting the list text " + "'" + keywordData.data + "'");

		try {
			if (!keywordData.data.equals(Constants.RANDOM_VALUE)) {
				Select sel = new Select(driver.findElement(By.xpath(keywordData.locator)));
				sel.selectByVisibleText(keywordData.data);
			} else {
				// logic to find a random value in list
				WebElement droplist = driver.findElement(By.xpath(keywordData.locator));
				List<WebElement> droplist_cotents = droplist.findElements(By.tagName("option"));
				Random randomNumber = new Random();
				int index = randomNumber.nextInt(droplist_cotents.size());
				String selectedVal = droplist_cotents.get(index).getText();
				driver.findElement(By.xpath(keywordData.locator)).sendKeys(selectedVal);
			}
		} catch (Exception e) {
			logger.debug("Exception in selectListText - ", e);

			extentTest.log(LogStatus.FAIL, "Could not select from list " + keywordData.locator
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " - Could not select from list. " + e.getMessage();

		}
		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS,
					"Selected value sucessfully" + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {

			extentTest.log(LogStatus.PASS, "Selected   value sucessfully");
		}

		return Constants.KEYWORD_PASS;
	}

	public String closeBrowser() {
		logger.info("Closing the browser");
		try {
			driver.close();
		} catch (Exception e) {
			logger.debug("Exception in closeBrowser - ", e);

			extentTest.log(LogStatus.FAIL, "Unable to close browser. Check if its open");
			return Constants.KEYWORD_FAIL + "Unable to close browser. Check if its open" + e.getMessage();
		}

		extentTest.log(LogStatus.PASS, "Browser closed successfully");
		return Constants.KEYWORD_PASS;

	}

	// Waits for given number of seconds (provided through data)
	public String pause() {
		logger.info("Waiting for" + " " + keywordData.data + " " + "secs");
		int time = (int) Double.parseDouble(keywordData.data);
		try {
			Thread.sleep(time * 1000);
		} catch (NumberFormatException | InterruptedException e) {
			extentTest.log(LogStatus.FAIL,"Please check the argurment passed for pause method. It should be a number in string format."	+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL;
		}

	//	extentTest.log(LogStatus.INFO, "Waiting for" + " " + keywordData.data + " " + "secs");
		return Constants.KEYWORD_PASS;
	}

	public String testsgDate() {
		Date d = new Date();
		String date = d.toString().replaceAll(" ", "_");
		date = date.replaceAll(":", "_");
		date = date.replaceAll("\\+", "_");
		return date;
	}

	// Captures the screen shot and saves in provided filename location
	public void captureScreenshot(String filename, String keyword_execution_result, String screenShotOnDemand)
			throws IOException {
		// take screen shots

		if (configProperties.getProperty("testLocation").equals("Local")) {
		}
		if (keyword_execution_result.startsWith(Constants.KEYWORD_FAIL)) {
			// capture screenshot
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(filename + ".png"));
		} else if (keywordData.screenShotOnDemand.equals("Y")) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(filename + ".png"));
		}
	}

	public String getOrder() {
		logger.info("getting order number");

		try {
			OrderNo = driver.findElement(By.xpath("//h2")).getText().substring(8, 12);

		} catch (Exception e) {
			logger.debug("Exception in getting order number - ", e);
			extentTest.log(LogStatus.FAIL,
					"Not able to get Order " + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + "Not able to get Order";
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS,
					"got order number" + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "got order number");
		}
		return Constants.KEYWORD_PASS;
	}

	public String gotoOrder() {
		logger.info("clicking on the Order number");
		try {
			driver.findElement(By.xpath("//a[contains(text(),'Order " + OrderNo + "')]")).click();
		} catch (Exception e) {
			extentTest.log(LogStatus.FAIL, "Not able to click " + keywordData.locator
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Not able to click";
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "Successfully clicked" + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "Clicked order successfully");
		}

		return Constants.KEYWORD_PASS;
	}

	public String selectOrderFromGrid() {
		logger.info("clicking on the Order number");
		try {
			driver.findElement(By.xpath("//a[contains(text(),'ov" + OrderNo + "')]")).click();
		} catch (Exception e) {
			extentTest.log(LogStatus.FAIL, "Not able to click " + keywordData.locator
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Not able to click";
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "Successfully clicked" + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "Clicked order successfully from grid");
		}

		return Constants.KEYWORD_PASS;
	}

	public String clickOnBeginConfiguration() {
		try {
			List<WebElement> rows = driver.findElement(By.xpath("//table[@class='simple-table']"))
					.findElements(By.tagName("tr"));
			int rowCount = rows.size();
			String rowNo = "";

			for (int i = 0; i < rowCount; i++) {
				WebElement row = driver
						.findElement(By.xpath("//table[@class='simple-table']/tbody/tr[" + (i + 1) + "]"));
				if (row.getText().trim().contains(OrderNo)) {
					// if(row.getText().trim().contains("2130")) {
					rowNo = Integer.toString(i + 1);
					break;
				}
			}

			System.out.println("**" + rowNo);

			driver.findElement(By.xpath("//tr[" + rowNo + "]/td[6]/button")).click();
		} catch (Exception e) {

			extentTest.log(LogStatus.FAIL,
					"Not able to click " + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
			return Constants.KEYWORD_FAIL + " Not able to click begin configuration";
		}

		if (keywordData.screenShotOnDemand.equals("Y")) {
			extentTest.log(LogStatus.PASS, "clicked Begin configuration" + keywordData.ObjectName
					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
		} else {
			extentTest.log(LogStatus.PASS, "Clicked Begin configuration successfully from grid");
		}

		return Constants.KEYWORD_PASS;

	}

//	/************************
//	 * Windows specific Keywords - AutoIt Methods
//	 ********************************/
//
//	// Runs the file using the file location provided in the data
//	public String autoItRun() {
//
//		logger.debug("Running " + "'" + keywordData.data + "'");
//		try {
//
//			AutoIt.run(keywordData.data);
//
//		} catch (Exception e) {
//			extentTest.log(LogStatus.FAIL,
//					"Failed to run" + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//			return Constants.KEYWORD_FAIL + " Not able to run autoIT command";
//
//		}
//		if (keywordData.screenShotOnDemand.equals("Y")) {
//			extentTest.log(LogStatus.PASS, "Ran Auto IT command" + keywordData.ObjectName
//					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//		} else {
//			extentTest.log(LogStatus.PASS, "Run auto IT - Failed");
//		}
//
//		return Constants.KEYWORD_PASS;
//	}
//
//	// Closes the application
//	public String autoItClose() {
//
//		logger.debug("Closing " + "'" + keywordData.data + "'");
//		try {
//			AutoIt.winClose(keywordData.data);
//
//		} catch (Exception e) {
//			extentTest.log(LogStatus.FAIL,
//					"Failed to run auto IT close  " + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//			return Constants.KEYWORD_FAIL + "Unable to close";
//		}
//		if (keywordData.screenShotOnDemand.equals("Y")) {
//			extentTest.log(LogStatus.PASS, " closed sucessfully", keywordData.screenShotPath + ".png");
//		} else
//			extentTest.log(LogStatus.PASS, " closed sucessfully");
//
//		return Constants.KEYWORD_PASS;
//	}
//
//	public String autoItActivate() {
//
//		logger.debug("'" + keywordData.data + "'" + " activated");
//		try {
//
//			AutoIt.winActivate(keywordData.data);
//			AutoIt.winWaitActive(keywordData.data);
//
//		} catch (Exception e) {
//			extentTest.log(LogStatus.FAIL, "Failed to run auto IT activate  "
//					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//			return Constants.KEYWORD_FAIL + " Not able to run autoIT command auto IT Activate";
//		}
//		if (keywordData.screenShotOnDemand.equals("Y")) {
//			extentTest.log(LogStatus.PASS, "Ran Auto IT command" + keywordData.ObjectName
//					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//		} else {
//			extentTest.log(LogStatus.PASS, "Run auto IT Actvated - Failed");
//		}
//		return Constants.KEYWORD_PASS;
//	}
//
//	public String autoItCtrlSetText() {
//		logger.debug("Entering text " + "'" + keywordData.data + "'");
//		try {
//			// object = keyValues.xmlp(locator);
//			String title = keywordData.locator.split(Constants.DATA_SPLIT)[0];
//			String text = keywordData.locator.split(Constants.DATA_SPLIT)[1];
//			String control = keywordData.locator.split(Constants.DATA_SPLIT)[2];
//			String string = keywordData.data;
//			AutoIt.winWait(title, text, 5);
//			AutoIt.ControlSetText(title, text, control, string);
//
//		} catch (Exception e) {
//			extentTest.log(LogStatus.FAIL, "Failed to run control set text "
//					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//			return Constants.KEYWORD_FAIL + " Not able to run autoIT command control set text";
//		}
//		if (keywordData.screenShotOnDemand.equals("Y")) {
//			extentTest.log(LogStatus.PASS,
//					"Ran Auto IT command" + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//		} else {
//			extentTest.log(LogStatus.FAIL, "Run auto IT control set text Failed");
//		}
//		return Constants.KEYWORD_PASS;
//	}
//
//	public String autoItSend() {
//
//		logger.debug("Sending keyboard imput " + "'" + keywordData.data + "'");
//		try {
//
//			AutoIt.send(keywordData.data);
//
//		} catch (Exception e) {
//			extentTest.log(LogStatus.FAIL,
//					"Failed to run  auto IT send " + extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//			return Constants.KEYWORD_FAIL + " Not able to run autoIT command auto IT send";
//
//		}
//		if (keywordData.screenShotOnDemand.equals("Y")) {
//			extentTest.log(LogStatus.PASS, "Ran Auto IT send command" + keywordData.ObjectName
//					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//		} else {
//			extentTest.log(LogStatus.PASS, "Run auto IT send - Failed");
//		}
//
//		return Constants.KEYWORD_PASS;
//	}
//
//	/************************
//	 * Windows specific Keywords - AutoIt Methods sdriver is a SikuliFirefoxDriver
//	 * to drive interactions with a web page using both image recognition (Sikuli)
//	 * and DOM (Selenium).
//	 * 
//	 ********************************/
//	public String sikuScrnRightClick() throws FindFailed, InterruptedException {
//		logger.debug("Right clicking on button ");
//
//		try {
//			keywordData.locator = sikuroot + keywordData.locator;
//			screen.find(keywordData.locator);
//			screen.rightClick(keywordData.locator);
//
//		} catch (Exception e) {
//			extentTest.log(LogStatus.FAIL, "Failed to run  sikuli right click"
//					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//			return Constants.KEYWORD_FAIL + " -- Not able to right click button" + e.getMessage();
//
//		}
//		if (keywordData.screenShotOnDemand.equals("Y")) {
//			extentTest.log(LogStatus.PASS, "Right Clicked Successfully" + keywordData.ObjectName
//					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//		} else {
//			extentTest.log(LogStatus.FAIL, "Right Click wit autoIT Failed");
//		}
//		return Constants.KEYWORD_PASS;
//
//	}
//
//	public String sikuScrnClick() throws FindFailed, InterruptedException {
//		logger.debug("Clicking on button ");
//		try {
//			keywordData.locator = sikuroot + keywordData.locator;
//			screen.find(keywordData.locator);
//			screen.click(keywordData.locator);
//
//		} catch (Exception e) {
//			extentTest.log(LogStatus.FAIL, "Failed to run  sikuli screen click"
//					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//			return Constants.KEYWORD_FAIL + " -- Not able to click button with sikuli" + e.getMessage();
//
//		}
//		if (keywordData.screenShotOnDemand.equals("Y")) {
//			extentTest.log(LogStatus.PASS, "sikuli Clicked Successfully" + keywordData.ObjectName
//					+ extentTest.addScreenCapture(keywordData.screenShotPath + ".png"));
//		} else {
//			extentTest.log(LogStatus.PASS, "sikuli Click successfull");
//		}
//
//		return Constants.KEYWORD_PASS;
//	}

}