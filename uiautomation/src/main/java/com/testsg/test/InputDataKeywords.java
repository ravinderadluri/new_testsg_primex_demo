package com.testsg.test;

import static io.restassured.RestAssured.given;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import io.restassured.response.Response;
import org.apache.log4j.Logger;

import com.cedarsoftware.util.io.JsonWriter;
import com.relevantcodes.extentreports.LogStatus;
import com.testsg.util.JsonFormat;

public class InputDataKeywords {
	final Logger logger = Logger.getLogger(InputDataKeywords.class);
	public RestApiData data = new RestApiData();
	
	public String Post() {
		Response responsePayload = null;
		logger.debug("Calling InputDataKeywords.Post()");
		if (!data.requestContentType.isEmpty() && data.requestBody.isEmpty() && !data.requestURL.isEmpty() && !data.requestHeaders.isEmpty())
			responsePayload = given().contentType(data.requestContentType).headers(data.requestHeaders).post(data.requestURL);
		else if (!data.requestURL.isEmpty() && !data.requestContentType.isEmpty() && !data.requestBody.isEmpty() && data.requestHeaders.isEmpty()) {
			logger.debug("Request URL = " + data.requestURL);
			logger.debug("Body = " + data.requestBody);
			logger.debug("Content-Type = " + data.requestContentType);
			responsePayload = 
				given()
				.contentType(data.requestContentType)
				.body(data.requestBody)
				.post(data.requestURL);
		}
		else if (!data.requestURL.isEmpty() && !data.requestContentType.isEmpty() && !data.requestBody.isEmpty() && !data.requestHeaders.isEmpty()) {
			logger.debug("Request URL = " + data.requestURL);
			logger.debug("Body = " + data.requestBody);
			logger.debug("Content-Type = " + data.requestContentType);
			logger.debug("Request Headers = " + data.requestHeaders.toString());
			responsePayload = given().contentType(data.requestContentType).headers(data.requestHeaders).body(data.requestBody).post(data.requestURL);
		}

		if (responsePayload.getBody().asString().contains("SUCCESS")) {
			if (responsePayload.jsonPath().getJsonObject("response.results.authenticationToken") != null) {
				data.requestHeaders.put("X-Agencyport-Token", responsePayload.jsonPath().getJsonObject("response.results.authenticationToken"));
				data.requestHeaders.put("X-Agencyport-CSRF-Token", responsePayload.getHeader("X-Agencyport-CSRF-Token"));
			}
			data.extentTest.log(LogStatus.PASS, "POST Successful response on URL = " + data.requestURL);
	    	data.responseJsonPayload = responsePayload.getBody().asString();
			return Constants.KEYWORD_PASS + " - Successful response | POST Request URL = " + data.requestURL;
	    }
	    data.extentTest.log(LogStatus.FAIL, "Failure response | POST Request URL = " + data.requestURL);
		return Constants.KEYWORD_FAIL + " - Failure response | POST Request URL = " + data.requestURL;
	}

	public String Get() {
		Response responsePayload = null;
		responsePayload = given().contentType(data.requestContentType).headers(data.requestHeaders).get(data.requestURL);
		if (responsePayload.getBody().asString().contains("SUCCESS")) {
			data.responseJsonPayload = responsePayload.getBody().asString();
			data.extentTest.log(LogStatus.PASS, "GET Request URL = " + data.requestURL);
			return Constants.KEYWORD_PASS + " - Successful response | GET Request URL = " + data.requestURL;
		}
		data.extentTest.log(LogStatus.FAIL, "GET Request URL = " + data.requestURL);
		return Constants.KEYWORD_FAIL + " - Failure response | GET Request URL = " + data.requestURL;
	}
	
	// modified the Request Body
	public String Body() {
		String reportFmtStart = "<textarea rows=\"4\" cols=\"150\" style=\"background-color: #f3f3f3;\" readonly>";
		String payload = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		if (payload.contains("|") && !data.requestBody.isEmpty()) {
			// Expecting format "JSON|response.results.section[3].fields[2].date=01/02/2017"
			String[] data1 = payload.split("\\s*\\|\\s*"); 
			if (data1[0].toUpperCase().startsWith("JSON")) {
				String[] data2 = data1[1].split("\\s*\\=\\s*");
				data.requestBody = JsonFormat.setJsonValueToPayload(data2[0], data2[1], data.requestBody);
				if (!data.requestBody.contains(data2[1])) {
					data.extentTest.log(LogStatus.FAIL, "Request Body = \n"+ reportFmtStart + JsonWriter.formatJson(data.requestBody)+"</textarea>");
					return Constants.KEYWORD_FAIL + " - Set Body to " + data.requestBody;
				}
			}
		}
		else
			data.requestBody = payload;
		data.extentTest.log(LogStatus.PASS, "Request Body = \n"+ reportFmtStart + JsonWriter.formatJson(data.requestBody)+"</textarea>");
		return Constants.KEYWORD_PASS + " - Set Body to " + data.requestBody;
	}
	
	public String ContentType() {
		data.requestContentType = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		data.extentTest.log(LogStatus.PASS, "Set Content-Type to " + data.requestContentType);
		return Constants.KEYWORD_PASS + " - Set Content-Type to " + data.requestContentType;
	}
	
	public String URL() {
		String url = data.requestURL;
		// Template URL containing substrings, "<<var>>" waiting to be filled 
		if (url.contains("<<")) {
			String key = "";
			String value = "";
			String regex = "\\<<(var)\\>>";
			// Get number of times "<<" is found inside the URL template
			int count = (url.length() - url.replace("<<", "").length()) / 2;
			for (int x = 0; x < count; x++) {
				// increment to the next column's cell, and get the data
				key = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum).trim();
				// if the key is inside the hashmap, then use the value to replace the URL "<<key>>"
				if (data.variables.containsKey(key)) {
					value = data.variables.get(key);
				}
				// Find the value using the "JSON|trunk.branch[4].leaf.key" path like from the internally stored JSON payload
				else if (key.contains("|")) {
					// Remove whitespace between the pipe symbol
					String[] data1 = key.split("\\s*\\|\\s*"); 
					if (key.toUpperCase().startsWith("JSON")) {
						value = JsonFormat.getValueFromKeyAndJsonPayload(data1[1], data.responseJsonPayload);
						// Fail on value containing just double-quotes ""
						if (value.contains("\\\"\\\"") || value.isEmpty()) {
							data.extentTest.log(LogStatus.FAIL, "JSON Response key = ["+key+"] = "+ value);
							return Constants.KEYWORD_FAIL + "JSON Response key = ["+key+"] = "+ value;
						}
					}
					// Process a cell containing "XML | <XMLpath><XMLbranch><XMLkey>"
					else if (key.toUpperCase().startsWith("XML")) {
						// TODO: XML Utility class needed for parsing and read/write key-value pairs on data.responseXmlPayload
						value = "\"\"";
						if (value.contains("\\\"\\\"") || value.isEmpty()) {
							data.extentTest.log(LogStatus.FAIL, "XML functionality is not implemented yet");
							return Constants.KEYWORD_FAIL + "XML functionality is not implemented yet";
						}
					}
				}
				// Allows for hard-coded names
				else {
					value = key.toString();
				}
				regex = regex.replace("var", key);
				url = url.replaceFirst(regex, value);
				logger.debug("regex = "+ regex);
				logger.debug("url = "+ url);
			}
			data.requestURL = url;
		}
		else
			data.requestURL = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum).trim();
		data.extentTest.log(LogStatus.PASS, "Request URL = " + data.requestURL);
		return Constants.KEYWORD_PASS + " - Request URL = " + data.requestURL;
	}
	
	// Represents placing JSON into Body
	public String Json() {
		// Move to the next column after the "Json" keyword to collect the cell data
		data.requestBody = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		data.requestContentType = "application/json";
		data.extentTest.log(LogStatus.PASS, "Data inserted into Request Body");
		return Constants.KEYWORD_PASS + " - Set REST Request Body(JSON) = " + data.requestBody;
	}
	
	// Represents placing XML into Body
	public String Xml() {
		// Move to the next column after the "Xml" keyword to collect the cell data
		data.requestBody = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		data.requestContentType = "application/xml";
		data.extentTest.log(LogStatus.PASS, "Data inserted into Request Body");
		return Constants.KEYWORD_PASS + " - Set REST Request Body(XML) = " + data.requestBody;
	}
	
	public String Header() {
		String hName = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		String hValue = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		if (hName.isEmpty() || hValue.isEmpty()) {
			data.extentTest.log(LogStatus.FAIL, "Header incorrectly defined");
			logger.debug(Constants.KEYWORD_FAIL + " - Header incorrectly defined");
			return Constants.KEYWORD_FAIL + " - Header incorrectly defined";
		}
		data.requestHeaders.put(hName, hValue);
		data.extentTest.log(LogStatus.PASS, "Adding a REST Request Header: " +  hName + " = " + data.requestHeaders.get(hValue));
		logger.debug(Constants.KEYWORD_PASS + " - Adding a REST Request Header: " +  hName + " = " + data.requestHeaders.get(hValue));
		return Constants.KEYWORD_PASS + " - Adding a REST Request Header: " +  hName + " = " + data.requestHeaders.get(hValue);
	}
	
	public String Token() {
		data.xlsColNum++;
		logger.debug(Constants.KEYWORD_PASS + " - TOKEN successful");
		return Constants.KEYWORD_PASS;
	}
	
	// Generate the FEIN # 
	// Cannot lookup in the database because it is encrypted
	// Modify the Effective and Expiration dates
	// Post the Request until successful
	public String AgentInitWorkItem() {
		String reportFmtStart = "<textarea rows=\"4\" cols=\"150\" style=\"background-color: #f3f3f3;\" readonly>";
		String data1 = data.xlsDataSheet.getCellData(data.xlsSheetTabName, ++data.xlsColNum, data.xlsRowNum);
		Calendar cal = GregorianCalendar.getInstance();
        Date today = cal.getTime();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = df.format(today);
        String yy = date.substring(2,4);
        String mm = date.substring(5,7);
        String dd = date.substring(8,10);
        String hh = date.substring(11,13);
        String ss = date.substring(17);
        int workItemId = 0;
        int z = 0;
        // override the default METHOD column verb
        data.overrideMethod = true;
        // Fein format = my-ddhhiii
        // month+year - DayHourIncrement
        String iii = "";
        String fein = "";
        
        String BaseURL = data.variables.get("BaseURL");
        String restoreBody = data.requestBody;
        // Agent init Post to have the system generate work item id
        for (int x = 0; x < 5; x++)
        {
        	data.requestBody = "";
        	data.requestURL = BaseURL + "/api/workerscomp/workflow/createQuoteSubmission?ACCOUNTID=1001&channel=AGENT&lob=WORK";
        	this.Post();
        	if (data.responseJsonPayload.contains("SUCCESS")) {
        		/// get id
	        	String jsonPath = "response.results.workItem.workItemId.value";
	        	workItemId = Integer.parseInt(JsonFormat.getValueFromKeyAndJsonPayload(jsonPath, data.responseJsonPayload));
	        	data.variables.put("wid", Integer.toString(workItemId));
        	}
        	data.requestBody = restoreBody;

	        df = new SimpleDateFormat("MM-dd-yyyy");
	        String effectiveDate = df.format(today).toString();
	        cal.add(Calendar.YEAR, 1);
	    	today = cal.getTime();
	        String expireDate = df.format(today).toString();
	        // Set the correct Effective and Expiration date
	        data.requestBody = JsonFormat.setJsonValueToPayload("page.sections[1].fields[0].value", effectiveDate, data.requestBody);
	        data.requestBody = JsonFormat.setJsonValueToPayload("page.sections[1].fields[0].defaultValue", effectiveDate, data.requestBody);
	        data.requestBody = JsonFormat.setJsonValueToPayload("page.sections[1].fields[2].value", expireDate, data.requestBody);
	        data.requestBody = JsonFormat.setJsonValueToPayload("page.sections[1].fields[2].defaultValue", expireDate, data.requestBody);
        
        	logger.debug("Perform Generate FEIN iteration = " +x+ " time(s)");
        	// generate a new fein number from scratch
        	int monthPlusYear = Integer.parseInt(mm) + Integer.parseInt(yy);
        	String strTemp = Integer.toString(monthPlusYear);
        	if (strTemp.length() > 2)
        		strTemp = strTemp.substring(1);
        	fein = strTemp + "-" + dd + hh + "000";

        	z = Integer.parseInt(fein.substring(7));
        	hh = fein.substring(5,7);
        	long milliseconds;
        	if (z >= 999) {
        		// increment the hh by 1, and reset the z to "000"
        		z = Integer.parseInt(hh);
        		z+=1;
       			hh = Integer.toString(z); 
        		fein = fein.substring(0,5) + hh + "000";
        	}
        	// increment is 998 or less
        	else {
        		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        		milliseconds = timestamp.getTime();
        		iii = Long.toString(milliseconds);
        		if (iii.length() > 3) {
        			iii = iii.substring(iii.length() - 3);
        		}
        		fein = fein.substring(0,7) + iii;
        	}

	        data.variables.put("fein", fein);
	        if (data1.contains("|")) {
	        	String[] key = data1.split("\\s*\\|\\s*");
	        	if (key[0].toUpperCase().equals("JSON")) {
	        		// JSON xpath inside Request body to change the FEIN number
	        		data.requestBody = JsonFormat.setJsonValueToPayload(key[1], fein, data.requestBody);
	        		logger.debug("modified payload = " + data.requestBody);
	        	}
	        	else if (key[0].toUpperCase().equals("XML")) {
	        		// TODO: XML xpath
	        		logger.warn("XML for fein genenation not implemented.");
	        	}
	        }

	        // Construct the URL and insert the work id
	        data.requestURL = BaseURL + data.xlsDataSheet.getCellData(Constants.TEST_STEPS_SHEET,  4, data.xlsRowNum);
	        data.requestURL = data.requestURL.replaceFirst("\\<<(wid)\\>>", Integer.toString(workItemId));
	        this.Post();
	        String error = JsonFormat.getValueFromKeyAndJsonPayload("response.results.viewInfo.messages[0].heading", data.responseJsonPayload);
	        logger.debug("POST Response payload = "+data.responseJsonPayload);
	        if (!error.equalsIgnoreCase("Error")) {
	        	data.extentTest.log(LogStatus.PASS, "POST Response payload = \n"+ reportFmtStart + JsonWriter.formatJson(data.responseJsonPayload)+"</textarea>");
	        	break;
	        }
	        data.requestBody = restoreBody;
        }
		logger.debug(Constants.KEYWORD_PASS + " - WorkItemId = ["+workItemId+"] | FEIN # = ["+fein+"]");
		return Constants.KEYWORD_PASS;
	}
	

}
