package com.testsg.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.Assert;
import org.testng.TestNG;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.testsg.util.ApiKeywordUtils;

public class DriverScript {

	public final static Logger logger = Logger.getLogger(DriverScript.class);
	public static String currentTestSuiteName;
	public static String currentTestCaseName;
	public static String dataSheetName;
	public static String inputFolderPath;
	public static int currentTestDataSetID = 2;
	public static Method methods[];
	public static Method desktopMethods[];
	public static Method capturescreenShot_method;
	public Method testsgDate_method;
	public static Keywords keywords;
	public static DesktopKeywords desktopkeywords;
	public static Method desktopCapturescreenShot_method;
	public static RestApiData restApiData = new RestApiData();
	public static ArrayList<String> resultSet;
	public static File testSuiteDataFile;
	public static File currentTestSuiteFile;
	public static Xls_Reader inputDataSheetXLS;
	public static Xls_Reader currentTestSuiteXLS;
	public static Xls_Reader suiteXLS;
	public static Properties configProperties;

	public static boolean UItest;
	public static Method inputDataMethods[];
	public static Method outputDataMethods[];
	public static InputDataKeywords inputDataKeywords;
	public static OutputDataKeywords outputDataKeywords;
	public static String BaseURL = "";

	public static long numberOfAsserts;
	static ExtentReports extrep = null;

	public DriverScript() throws NoSuchMethodException, SecurityException {
		keywords = new Keywords();
		desktopkeywords = new DesktopKeywords();
		methods = keywords.getClass().getMethods();
		desktopMethods = desktopkeywords.getClass().getMethods();
		capturescreenShot_method = keywords.getClass().getMethod(Constants.CAPTURESCREENSHOT, String.class,String.class, String.class);
		desktopCapturescreenShot_method = desktopkeywords.getClass().getMethod(Constants.CAPTURESCREENSHOT, String.class, String.class,String.class);
		testsgDate_method = keywords.getClass().getMethod("testsgDate");
		ApiKeywordUtils.numberOfAsserts = numberOfAsserts = 0;

		UItest = true;
		ApiKeywordUtils.inputDataKeywords = inputDataKeywords = new InputDataKeywords();
		ApiKeywordUtils.inputDataMethods = inputDataMethods = inputDataKeywords.getClass().getMethods();
		ApiKeywordUtils.outputDataKeywords = outputDataKeywords = new OutputDataKeywords();
		ApiKeywordUtils.outputDataMethods = outputDataMethods = outputDataKeywords.getClass().getMethods();
	}

	private static String getFolderPath(String filePath) {
		File file = new File(System.getProperty(Constants.USERDIR) + File.separator + filePath);
		if (Constants.REPORTS.equalsIgnoreCase(filePath) || Constants.TOOLS.equalsIgnoreCase(filePath)) {
			if (!file.exists())
				file.mkdirs();
		}
		return file.getAbsolutePath();
	}


	@Test
	@Parameters({ "SuiteFileName", "browserType", "hubURL", "env_url" })
	public static void runTests(String SuiteFile, @Optional("") String browserType, @Optional("") String hubURL,
			@Optional("") String env_url) throws Exception 
		{
		String outputFolderPath = null;	
		String configFolderPath = null;
		FileInputStream fs = null;
		BaseURL = env_url;
		System.out.println("Executing " +SuiteFile);

		try {
			inputFolderPath = getFolderPath(Constants.INPUTFOLDER);
			outputFolderPath = getFolderPath(Constants.REPORTS);
			configFolderPath = getFolderPath(Constants.CONFIGFOLDER);
			String resultsfolderpath = getResultFolderPath(browserType, hubURL, env_url, outputFolderPath);
			String path = resultsfolderpath + File.separator + Constants.AUTOMATIONREPORT
					;
			// Create new instance of Extent report with file path and loading the extent reports config file
			extrep = new ExtentReports(path);

			extrep.loadConfig(new File(configFolderPath + File.separator + Constants.REPORTXML));

			PropertyConfigurator.configure("log.properties");

			File configFile = new File(configFolderPath, Constants.CONFIGXML);

			if (!configFile.exists()) {
				logger.info("config.xml is not found at " + configFolderPath);
				System.exit(0);
			}

			fs = new FileInputStream(configFile.getAbsoluteFile());
			configProperties = new Properties();
			configProperties.loadFromXML(fs);

			logger.info("Properties loaded. Starting testing");
			logger.info("Intialize Suite xlsx");

			try {
				File suiteFile = new File(inputFolderPath, SuiteFile);
				if (!suiteFile.exists()) {
					logger.info("Suite.xlsx is not found at " + inputFolderPath);
					System.exit(0);
				}

				suiteXLS = new Xls_Reader(suiteFile.getAbsolutePath());
				readSheets(resultsfolderpath, suiteXLS);
			} catch (Exception e) {
				logger.info("Exception occured - ", e);
				numberOfAsserts++;
			}

		} catch (FileNotFoundException e) {
			logger.error("FileNotFoundException  - ", e);
			numberOfAsserts++;
		} catch (IOException e) {
			logger.error("IOException  - ", e);
			numberOfAsserts++;
		} catch (Exception e) {
			logger.error("Exception  - ", e);
			numberOfAsserts++;
		} finally {
			fs.close();
		}
		
		// Assertion for Bamboo
		numberOfAsserts += ApiKeywordUtils.numberOfAsserts;
		numberOfAsserts += resultSet.stream().filter(s -> s.contains(Constants.KEYWORD_FAIL)).count();
		Assert.assertTrue(numberOfAsserts == 0, "TestNG detected " + numberOfAsserts + " Failed tests");
	}

	private static String getResultFolderPath(String browserType, String hubURL, String env_url,String outputFolderPath) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		DriverScript driverScript = new DriverScript();
		keywords.keywordData.browserType = browserType;
		keywords.keywordData.hubURL = hubURL;
		keywords.keywordData.navURL = env_url;
		String resultsfolderpath = driverScript.createhtmlPath(outputFolderPath);
		return resultsfolderpath;
	}

	private static void readSheets(String resultsfolderpath, Xls_Reader suiteXLS)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		for (int currentSuiteID = 2; currentSuiteID <= suiteXLS
				.getRowCount(Constants.TEST_SUITE_SHEET); currentSuiteID++) {
			currentTestSuiteName = suiteXLS.getCellData(Constants.TEST_SUITE_SHEET, Constants.TEST_CASE_ID,
					currentSuiteID);

			if (currentTestSuiteName.contains("N/A") || currentTestSuiteName.isEmpty()) {
				UItest = false;
				
				// ACTION column is ignored, use DATA column
				logger.info("***Begin REST API Test Suite***"
						+ suiteXLS.getCellData(Constants.TEST_SUITE_SHEET, Constants.DATA_SHEET, currentSuiteID));
			} else {
				UItest = true;
				// execute the test cases in the suite
				logger.info("Executing the Suite->" + currentTestSuiteName);
			}

			if (suiteXLS.getCellData(Constants.TEST_SUITE_SHEET, Constants.RUNMODE, currentSuiteID)
					.equals(Constants.RUNMODE_YES)) {

				currentTestSuiteFile = new File(inputFolderPath, currentTestSuiteName + ".xlsx");

				if (!currentTestSuiteFile.exists()) {
					logger.info(currentTestSuiteName + ".xlsx is not found at " + inputFolderPath);
					numberOfAsserts++;
					System.exit(0);
				}

				//testSuiteDataFile = new File(inputFolderPath, dataSheetName + ".xlsx");
				currentTestSuiteXLS = new Xls_Reader(currentTestSuiteFile.getAbsolutePath());
				//inputDataSheetXLS = new Xls_Reader(testSuiteDataFile.getAbsolutePath());
				readSheetData(resultsfolderpath);
			}
		}
	}

	private static void readSheetData(String resultsfolderpath)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		if (UItest == false) {
			// No longer using the "Test Cases" Sheet TAB
			currentTestCaseName = inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 1, 1);
			// Provide a name for the Test Plan
			restApiData.extentTest = extrep.startTest(currentTestCaseName, "Executing Test Case");
			// Store the Excel sheet to retrieve the data cells from inside the keyword
			// methods
			outputDataKeywords.data.xlsSheetTabName = inputDataKeywords.data.xlsSheetTabName = Constants.TEST_STEPS_SHEET;
			outputDataKeywords.data.xlsDataSheet = inputDataKeywords.data.xlsDataSheet = inputDataSheetXLS;
			resultSet = new ArrayList<String>();
			runRestApiKeywords(resultsfolderpath);
			extrep.endTest(restApiData.extentTest);
			extrep.flush();
		} else {
			for (int currentTestCaseID = 2; currentTestCaseID <= currentTestSuiteXLS.getRowCount(Constants.TEST_CASES_SHEET); currentTestCaseID++) 
			{
				currentTestCaseName = currentTestSuiteXLS.getCellData(Constants.TEST_CASES_SHEET, Constants.TCID,currentTestCaseID);
				keywords.extentTest = extrep.startTest(currentTestCaseName, "Executing Test Case");

				// Check the TC Runmode is "Y"		
				if (currentTestSuiteXLS.getCellData(Constants.TEST_CASES_SHEET, Constants.RUNMODE, currentTestCaseID).equals(Constants.RUNMODE_YES)) {
					logger.info("Executing the test case -> " + currentTestCaseName);	
					// iterating through all keywords
					resultSet = new ArrayList<String>();				
					logger.info(resultSet);
					executeKeywords(resultsfolderpath);
					createXLSReport();

				} else { 				
					// TC Runmode is "N" (also known as skip the test case)
					logger.info("Skipping the test case -> " + currentTestCaseName);
					keywords.extentTest.log(LogStatus.SKIP, "Skipping test case");
				}
				// End tests - Extent reports.
				extrep.endTest(keywords.extentTest);
				extrep.flush();
			}
		}
	}

	private static boolean runRestApiPreconditions() {
		inputDataKeywords.data.extentTest = restApiData.extentTest;
		// Refresh a copy of the Class's data
		ApiKeywordUtils.inputDataKeywords = inputDataKeywords;
		ApiKeywordUtils.outputDataKeywords = outputDataKeywords;
		// Store the BaseURL
		if (inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 0, 2).equals("BaseURL") && BaseURL.isEmpty())
			BaseURL = inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 1, 2);
		// Remove the BaseURL ending "/" character if it exists
		if (BaseURL.endsWith("/"))
			BaseURL = BaseURL.substring(0, BaseURL.lastIndexOf("/"));
		// Store the BaseURL for the keywords to use later
		inputDataKeywords.data.variables.put("BaseURL", BaseURL);
		restApiData.variables = inputDataKeywords.data.variables;

		// Run the Authentication URL
		if (inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 0, 3).startsWith("Authentication")) {
			inputDataKeywords.data.requestURL = inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 1, 3);
			inputDataKeywords.data.requestContentType = inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 2, 3);
			inputDataKeywords.data.requestBody = inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 3, 3);
			if (ApiKeywordUtils.runInputDataKeyword("Post").startsWith("FAIL")) {
				logger.info("Post method for Authentication URL request Failed");
				inputDataKeywords.data.extentTest.log(LogStatus.FAIL,
						"Post method for Authentication URL request Failed");
				restApiData.extentTest = inputDataKeywords.data.extentTest;
				numberOfAsserts++;
				return false;
			}
			logger.info("Retrieved Authentication URL request data Successful");
		}
		restApiData.extentTest = inputDataKeywords.data.extentTest;
		return true;
	}

	public static void runRestApiKeywords(String resultsfolderpath) {
		int colNum = 0;
		int rowNum = 5;
		String requestMethod = "";
		String urlPath = "";
		String currentKeyword = "";
		String keyword_execution_result = "";

		// Run Precondition Setup and Authentication
		if (!runRestApiPreconditions())
			return;

		// Begin to increment through the title bar, check that the title bar exists
		while (!inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 0, rowNum).isEmpty()) {
			// Increment to the next row to retrieve the data cell
			rowNum++;
			inputDataKeywords.data.overrideMethod = false;
			// Check that the next row to be processed is not blank
			if (inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 0, rowNum).isEmpty())
				continue;
			inputDataKeywords.data.extentTest = restApiData.extentTest;
			inputDataKeywords.data.xlsRowNum = rowNum;
			// Get the Request Method
			requestMethod = inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 3, rowNum);
			urlPath = inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 4, rowNum);
			inputDataKeywords.data.requestURL = BaseURL + urlPath;

			// Check RUN for 'Y' or 'N'
			if (inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 5, rowNum).equalsIgnoreCase("N")) {
				logger.info("Skipping the test case -> " + currentTestCaseName);
				inputDataKeywords.data.extentTest.log(LogStatus.SKIP,
						"SKIP TEST ID: ****************************************** "
								+ inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 0, rowNum)
								+ " ******************************************");
				continue;
			} else {
				inputDataKeywords.data.extentTest.log(LogStatus.INFO,
						"BEGIN TEST ID: ****************************************** "
								+ inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, 0, rowNum)
								+ " ******************************************");
			}
			// Skip ahead to the input for Request
			colNum = 6;
			logger.info("Begin reading input cell");
			// Search for InputDataKeyword methods in each column cell until reach the
			// "OutputData" keyword
			while (!inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, colNum, rowNum).equals("OutputData")) {
				currentKeyword = inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, colNum, rowNum);
				// Check for InputData method
				if (ApiKeywordUtils.isInputDataKeyword(currentKeyword)) {
					do {
						inputDataKeywords.data.xlsColNum = colNum;
						keyword_execution_result = ApiKeywordUtils.runInputDataKeyword(currentKeyword);
						logger.info(keyword_execution_result);
						colNum = inputDataKeywords.data.xlsColNum;
						colNum++;
						currentKeyword = inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, colNum, rowNum);
						// get the cell's data to be processed until the OutputData keyword is reached
					} while (!currentKeyword.equals("OutputData"));
					// Should the OutputData keyword in the cell be reached, stop the InputData
					// processing
					if (currentKeyword.equals("OutputData"))
						continue;
				}
				colNum++;
			}

			if (inputDataKeywords.data.overrideMethod == false) {
				// Construct the Request URL, call the Post/Get method
				requestMethod = requestMethod.substring(0, 1).toUpperCase() + requestMethod.substring(1).toLowerCase();
				keyword_execution_result = ApiKeywordUtils.runInputDataKeyword(requestMethod);
				logger.info(keyword_execution_result);
			}
			outputDataKeywords.data = inputDataKeywords.data;
			colNum++;
			logger.info("OutputData cell reached");
			// infinite loop starting in OutputData for each column cell until reach an
			// empty cell
			while (!inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, colNum, rowNum).isEmpty()) {
				// VerifyTrue or VerifyFalse
				currentKeyword = inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, colNum, rowNum);
				// looking for keyword after OutputData cell
				if (ApiKeywordUtils.isOutputDataKeyword(currentKeyword)) {
					// infinite loop to run each key-value pair verification based on keyword
					do {
						outputDataKeywords.data.xlsColNum = colNum;
						keyword_execution_result = ApiKeywordUtils.runOutputDataKeyword(currentKeyword);
						colNum = outputDataKeywords.data.xlsColNum;
						logger.info(keyword_execution_result);
						colNum++;
						currentKeyword = inputDataSheetXLS.getCellData(Constants.TEST_STEPS_SHEET, colNum, rowNum);
					} while (ApiKeywordUtils.isOutputDataKeyword(currentKeyword) && !currentKeyword.isEmpty());
				}
				colNum++;
			}
			restApiData = inputDataKeywords.data = outputDataKeywords.data;
			colNum = 0;
		}
		logger.info("Excel Sheet reached an Empty Row");

	}

	public static void executeKeywords(String resultsfolderpath) throws IllegalAccessException,
	IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		
		// iterating through all keywords
		String scrcpath = null;
		int currentTestStepID;
		String currentKeyword = "";
		String proceedOnFail = "";
		String keyword_execution_result = "";
		String desktop_execution_result="";

		//int index = -1;

		try {

			List<String> desktopList = new ArrayList<String>();
			for(int i =0;i<desktopMethods.length;i++){
				desktopList.add(desktopMethods[i].getName());
			}

			for (currentTestStepID = 2; currentTestStepID <= currentTestSuiteXLS.getRowCount(Constants.TEST_STEPS_SHEET); currentTestStepID++)
			{
				// checking TCID
				if (currentTestCaseName.equals(currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET,	Constants.TCID, currentTestStepID))) 
				{
					keywords.keywordData.data = currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET,Constants.DATA, currentTestStepID);

					if (keywords.keywordData.data.startsWith(Constants.DATA_START_COL)) {							
						keywords.keywordData.data = currentTestSuiteXLS.getCellData(currentTestCaseName,keywords.keywordData.data.split(Constants.DATA_SPLIT)[1], currentTestDataSetID);
						// read actual data value from the corresponding column
					}

					keywords.keywordData.locator = currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET,Constants.OBJECT, currentTestStepID);
					currentKeyword = currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, Constants.KEYWORD,	currentTestStepID);

					proceedOnFail = currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET,
							Constants.PROCEED_ON_FAIL, currentTestStepID);

					keywords.keywordData.screenShotOnDemand = currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, Constants.SCRNSHOTONDEMAND, currentTestStepID);

					keywords.keywordData.ObjectName = currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET,Constants.OBJNAME, currentTestStepID);

					// code to execute the keywords
					// Within methods array, find the matching method name, and return the array's index
					//index = Arrays.stream(methods).map(s -> s.getName()).collect(Collectors.toList()).indexOf(currentKeyword);



					for (int index = 0; index < methods.length ; index++) {
						if (methods[index].getName().equals(currentKeyword)) {
							keywords.keywordData.screenShotPath = "." + File.separator + "screenshots" + File.separator
									+ currentTestSuiteName + "_" + currentTestCaseName + "_TS" + currentTestStepID + "_"
									+ (currentTestDataSetID - 1);

							scrcpath = resultsfolderpath + File.separator + "screenshots" + File.separator
									+ currentTestSuiteName + "_" + currentTestCaseName + "_TS" + currentTestStepID + "_"
									+ (currentTestDataSetID - 1);

							keyword_execution_result = (String) methods[index].invoke(keywords);

							resultSet.add(keyword_execution_result);

							// capture screenshot
							capturescreenShot_method.invoke(keywords, scrcpath, keyword_execution_result,keywords.keywordData.screenShotOnDemand);



							// output data is written in output
							if (keyword_execution_result.startsWith("PASS") && currentKeyword.contains("output")) {
								currentTestSuiteXLS.setCellData(Constants.TEST_STEPS_SHEET, Constants.OUTPUT_DATA,currentTestStepID, resultSet.get(index).substring(4));
								break;
							}

							// Terminate if test step is failed and 'proceed_on_fail' flag is 'N'
							if ((keyword_execution_result.startsWith("FAIL") || keyword_execution_result.startsWith("SKIP")
									|| keyword_execution_result.startsWith(" ")) && proceedOnFail.equals("N")) 
							{
								keywords.closeBrowser();
								numberOfAsserts++;
								break;

							} else if (keyword_execution_result.startsWith("FAIL")) { // Log assert
								numberOfAsserts++;
							}
						}

						else if(desktopList.contains(currentKeyword)) {

							desktopkeywords.keywordData.screenShotPath = "." + File.separator + "screenshots" + File.separator
									+ currentTestSuiteName + "_" + currentTestCaseName + "_TS" + currentTestStepID + "_"
									+ (currentTestDataSetID - 1);

							scrcpath = resultsfolderpath + File.separator + "screenshots" + File.separator
									+ currentTestSuiteName + "_" + currentTestCaseName + "_TS" + currentTestStepID + "_"
									+ (currentTestDataSetID - 1);

							desktop_execution_result = (String) desktopMethods[desktopList.indexOf(currentKeyword)].invoke(desktopkeywords);

							logger.debug(desktop_execution_result);

							resultSet.add(desktop_execution_result);

							// capture screenshot			
							desktopCapturescreenShot_method.invoke(desktopkeywords, scrcpath, desktop_execution_result, desktopkeywords.keywordData.screenShotOnDemand);


							// output data is written in output
							if (desktop_execution_result.startsWith("PASS") && currentKeyword.contains("output")) {
								currentTestSuiteXLS.setCellData(Constants.TEST_STEPS_SHEET, Constants.OUTPUT_DATA,currentTestStepID, resultSet.get(index).substring(4));
								break;
							}

							// Terminate if test step is failed and 'proceed_on_fail' flag is 'N'
							if ((desktop_execution_result.startsWith("FAIL") || desktop_execution_result.startsWith("SKIP")
									|| desktop_execution_result.startsWith(" ")) && proceedOnFail.equals("N")) 
							{
								keywords.closeBrowser();
								numberOfAsserts++;
								break;

							} else if (desktop_execution_result.startsWith("FAIL")) { // Log assert
								numberOfAsserts++;
							}
						}
					}

				}

			} // end for loop

		}// end try

		catch (Exception e) {
			logger.info("Exception in executeKeywords method  - ", e);
			numberOfAsserts++;
		}
	}

	public String createhtmlPath(String outputFolderPath)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		Date d = new Date();
		String date = d.toString().replaceAll(" ", "_");
		date = date.replaceAll(":", "_");
		date = date.replaceAll("\\+", "_");
		String reportsDirPath = outputFolderPath + File.separator;
		String result_FolderName = null;
		String scrsht_FolderName = null;
		result_FolderName = reportsDirPath + Constants.REPORTS + "_" + date;
		new File(result_FolderName).mkdirs();
		scrsht_FolderName = result_FolderName + File.separator + Constants.SCREENSHOTS;
		new File(scrsht_FolderName).mkdirs();
		return result_FolderName;

	}

	public static void createXLSReport() {

		String colName = Constants.RESULT + (currentTestDataSetID - 1);
		boolean isColExist = false;
		int index = 0;
		try {
			for (int columnIndex = 0; columnIndex < currentTestSuiteXLS
					.getColumnCount(Constants.TEST_STEPS_SHEET); columnIndex++) {
				if (currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, columnIndex, 1).equals(colName)) {
					isColExist = true;
					break;
				}
			}

			if (!isColExist)
				currentTestSuiteXLS.addColumn(Constants.TEST_STEPS_SHEET, colName);

			for (int rowIndex = 2; rowIndex <= currentTestSuiteXLS
					.getRowCount(Constants.TEST_STEPS_SHEET); rowIndex++) {
				if (currentTestCaseName.equals(
						currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, Constants.TCID, rowIndex))) {
					if (resultSet.size() == 0)
						currentTestSuiteXLS.setCellData(Constants.TEST_STEPS_SHEET, colName, rowIndex,
								Constants.KEYWORD_SKIP);
					else if (index < resultSet.size()) {
						currentTestSuiteXLS.setCellData(Constants.TEST_STEPS_SHEET, colName, rowIndex,
								resultSet.get(index));

					} else {
						break;
					}
					index++;
				}
			}

			if (resultSet.size() == 0) {
				// skip
				currentTestSuiteXLS.setCellData(currentTestCaseName, Constants.RESULT, currentTestDataSetID,
						Constants.KEYWORD_SKIP);
				return;
			} else {
				for (int i = 0; i < resultSet.size(); i++) {
					if (!resultSet.get(i).equals(Constants.KEYWORD_PASS)) {
						currentTestSuiteXLS.setCellData(currentTestCaseName, Constants.RESULT, currentTestDataSetID,
								resultSet.get(i));
						return;
					}
				}
			}
			currentTestSuiteXLS.setCellData(currentTestCaseName, Constants.RESULT, currentTestDataSetID,
					Constants.KEYWORD_PASS);
		} catch (Exception e) {
			logger.info("Exception occured - ", e);
		}

	}

	public static void main(String arg[]) {
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { DriverScript.class });
		testng.run();
	}
}