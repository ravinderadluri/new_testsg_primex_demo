package com.testsg.test;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.testsg.util.JsonFormat;


public class RestApiKeywords {
	final Logger logger = Logger.getLogger(RestApiKeywords.class);
	JsonFormat jsonFormat = new JsonFormat();
	public RestApiData restApiData = new RestApiData();
	
//	public String sendPostReqAuthToken() {
//		// calling function directly without using other keyword, must populate the required fields
//		if (!restApiData.DataObjects[0].isEmpty() && !restApiData.DataObjects[1].isEmpty() && !restApiData.DataObjects[2].isEmpty()) {
//			restApiData.requestURL = restApiData.DataObjects[0].trim();
//			restApiData.requestContentType = restApiData.DataObjects[1].trim();
//			restApiData.requestBody = restApiData.DataObjects[2].trim();
//		}
//		if (!restApiData.requestContentType.isEmpty() && !restApiData.requestBody.isEmpty() && !restApiData.requestURL.isEmpty()) {
//			restApiData.getResponsePayload = 
//					given()
//					.contentType(restApiData.requestContentType)
//					.body(restApiData.requestBody)
//					.post(restApiData.requestURL);
//			restApiData.requestHeaders.clear();
//			restApiData.requestHeaders.put("X-Agencyport-Token", restApiData.getResponsePayload.jsonPath().getJsonObject("response.results.authenticationToken"));
//			restApiData.requestHeaders.put("X-Agencyport-CSRF-Token", restApiData.getResponsePayload.getHeader("X-Agencyport-CSRF-Token"));
//		}
//		else {
//			restApiData.extentTest.log(LogStatus.FAIL, "Content-Type / RequestBody / URL strings must be set before proceeding.");
//			return Constants.KEYWORD_FAIL + " - Content-Type / RequestBody / URL strings must be set before proceeding";
//		}
//
//	    if (restApiData.getResponsePayload.getBody().asString().contains("SUCCESS")) {
//	    	jsonFormat.storedJsonPayload = restApiData.getResponsePayload.getBody().asString();
//	    	restApiData.extentTest.log(LogStatus.PASS, "Successful response that Authentication Tokens recieved.");
//			return Constants.KEYWORD_PASS + " - Successful response | Request URL = " + restApiData.requestURL;
//	    }
//	    restApiData.extentTest.log(LogStatus.FAIL, "Failure response | Request URL = " + restApiData.requestURL);
//		return Constants.KEYWORD_FAIL + " - Failure response | Request URL = " + restApiData.requestURL;
//	}
//
//	// Set Request Body
//	public String setReqBody() {
//		restApiData.requestBody = restApiData.DataObjects[0];
//		restApiData.extentTest.log(LogStatus.PASS, "Data inserted into Request Body");
//		return Constants.KEYWORD_PASS + " - Set REST Request Body = " + restApiData.requestBody;
//	}
//
//	// Using the stored payload, find the nested key, and set the request body as the value
//	public String setReqBodyFromJsonValue() {
//		return Constants.KEYWORD_FAIL;
//	}
//	
//	public String setVar() {
//		// DATA "VAR[xxxx]=3004"  the xxxx depicts the string for dictionary
//		// this was designed for the workitemid or other items retrieved from the payload
//		return Constants.KEYWORD_PASS;
//	}
//	
//	// Using the stored payload, find the nested key, get the key's value and set it to the temporary variable
//	// DATA = "VAR[workitemid]=response.results.section[2].filter[5].value"
//	// restApiData.DataObjects[0] = workitemid
//	// restApiData.DataObjects[1] = response.results.section[2].filter[5].value
//	public String setVarFromJsonValue() {
//		String payload = jsonFormat.getJsonFromKeyAndPayload(restApiData.DataObjects[1], restApiData.getResponsePayload.asString());
//		if (payload == null || payload.isEmpty()) {
//			restApiData.extentTest.log(LogStatus.FAIL, "JSON Payload is not defined, nothing processed.");
//			return Constants.KEYWORD_FAIL + " - setVarFromJsonValue() returned No value in payload";
//		}
//		restApiData.variables.put(restApiData.DataObjects[0], payload);
//		restApiData.extentTest.log(LogStatus.PASS, "JSON Payload stored internally");
//		return Constants.KEYWORD_PASS;
//	}
//	
//	// Using the Stored Variable at position from "VAR[key]"
//	public String setURLFromVar() {
//		String key = restApiData.DataObjects[0];
//		restApiData.DataObjects[0] = restApiData.variables.get(key);
//		return this.setRequestURL();
//	}
//	
//	// Using the Stored Variable at position from "KV=<0>=VAR[key]"
//	// restApiData.DataObjects[0] = "<0>"
//	// restApiData.DataObjects[1] = "VAR[key]"
//	public String setURLtemplateFromVar() {
//		String key = restApiData.DataObjects[1];
//		restApiData.DataObjects[0] = restApiData.variables.get(key);
//		if (restApiData.DataObjects.length == 2)
//			return this.setURLtemplateVar();
//		restApiData.extentTest.log(LogStatus.FAIL, "Key-Value Pair [" +key+ "] not internally stored.");
//		return Constants.KEYWORD_FAIL;
//	}
//	
//	// Using existing Stored Variable, set the value as the Request Body
//	// restApiData.DataObjects[0] = key to find in the variables hashmap
//	public String setVarToReqBody() {
//		restApiData.requestBody = restApiData.variables.get(restApiData.DataObjects[0]);
//		if (restApiData.requestBody == null) {
//			restApiData.extentTest.log(LogStatus.FAIL, "Temporary Varable cannot find the key = " + restApiData.DataObjects[0]);
//			return Constants.KEYWORD_FAIL + " - Temporary Varable cannot find the key = " + restApiData.DataObjects[0];
//		}
//		restApiData.extentTest.log(LogStatus.PASS, "Temporary Varable found the key = " + restApiData.DataObjects[0]);
//		return Constants.KEYWORD_PASS + " - Temporary Varable found the key = " + restApiData.DataObjects[0];
//	}
//	
//	// Add individual request header
//	public String addReqHeader() {
//		String[] header = restApiData.DataObjects[0].split("=");
//		if (header.length < 2) {
//			restApiData.extentTest.log(LogStatus.FAIL, "Key-Value Pair not defined");
//			return Constants.KEYWORD_FAIL + " - cannot find parameter = value";
//		}
//		restApiData.requestHeaders.put(header[0], header[1]);
//		restApiData.extentTest.log(LogStatus.PASS, "Adding a REST Request Header: " +  header[0] + " = " + restApiData.requestHeaders.get(header[1]));
//		return Constants.KEYWORD_PASS + " - Adding a REST Request Header: " +  header[0] + " = " + restApiData.requestHeaders.get(header[1]);
//	}
//
//	// Remove individual request header
//	public String removeReqHeader() {
//		restApiData.requestHeaders.remove(restApiData.DataObjects[0]);
//		restApiData.extentTest.log(LogStatus.PASS, "Removing a REST Request Header = " + restApiData.DataObjects[0]);
//		return Constants.KEYWORD_PASS + " - Removing a REST Request Header = " + restApiData.DataObjects[0];
//	}
//
//	// Clear the request header list
//	public String removeAllReqHeaders() {
//		restApiData.requestHeaders.clear();
//		restApiData.extentTest.log(LogStatus.PASS, "Clearing all REST Request Headers");
//		return Constants.KEYWORD_PASS + " - Clearing all REST Request Headers";
//	}
//
//	public String setURLtemplateVar() {
//		restApiData.requestURL = restApiData.requestURL.replaceFirst(restApiData.DataObjects[0], restApiData.DataObjects[1]);
//		restApiData.extentTest.log(LogStatus.PASS, "Within URL template, replaced [" + restApiData.DataObjects[0] + "] with [" + restApiData.DataObjects[1] + "]");
//		return Constants.KEYWORD_PASS + " - Within URL template, replaced [" + restApiData.DataObjects[0] + "] with [" + restApiData.DataObjects[1] + "]";
//	}
//	
//	// Define a URL template with items to be populated
//	public String setURLtemplate() {
//		restApiData.requestURL = restApiData.DataObjects[0];
//		restApiData.extentTest.log(LogStatus.PASS, "Setting URL template = " + restApiData.requestURL);
//		return Constants.KEYWORD_PASS + " - Setting URL template = " + restApiData.requestURL;
//	}
//
//	// Set Request header's Content-Type
//	public String setReqHeaderContentType() {
//		restApiData.requestContentType = restApiData.DataObjects[0];
//		restApiData.extentTest.log(LogStatus.PASS, "Set Request Content-Type to " + restApiData.requestContentType);
//		return Constants.KEYWORD_PASS + " - Set Request Content-Type to " + restApiData.requestContentType;
//	}
//
//	public String sendGetURLtemplate() {
//		// before sending the GET request, make sure that the request header, content-type, or body has data.
//		// the attributes that have data assumes that it is needed to be sent for the request.
//		if (restApiData.requestURL.isEmpty())
//			if (restApiData.DataObjects[0].isEmpty()) {
//				restApiData.extentTest.log(LogStatus.FAIL, "URL template is not defined");
//				return Constants.KEYWORD_FAIL + " - URL template is not defined";
//			}
//			else
//				restApiData.requestURL = restApiData.DataObjects[0]; 
//
//		if (!restApiData.requestContentType.isEmpty() && !restApiData.requestBody.isEmpty() && restApiData.requestHeaders.isEmpty())
//			restApiData.getResponsePayload = given().contentType(restApiData.requestContentType).body(restApiData.requestBody).get(restApiData.requestURL);
//		else if (!restApiData.requestContentType.isEmpty() && !restApiData.requestHeaders.isEmpty() && restApiData.requestBody.isEmpty())
//			restApiData.getResponsePayload = given().contentType(restApiData.requestContentType).headers(restApiData.requestHeaders).get(restApiData.requestURL);
//		else if (!restApiData.requestContentType.isEmpty() && !restApiData.requestHeaders.isEmpty() && !restApiData.requestBody.isEmpty())
//			restApiData.getResponsePayload = given().contentType(restApiData.requestContentType).body(restApiData.requestBody).headers(restApiData.requestHeaders).get(restApiData.requestURL);
//
//		if (restApiData.getResponsePayload.getBody().asString().contains("SUCCESS")) {
//			jsonFormat.storedJsonPayload = restApiData.getResponsePayload.getBody().asString();
//			restApiData.extentTest.log(LogStatus.PASS, "GET " + restApiData.requestURL + " returned a payload.");
//			return Constants.KEYWORD_PASS + " - Successful response | Request URL = " + restApiData.requestURL;
//		}
//		restApiData.extentTest.log(LogStatus.FAIL, "GET Request URL = " + restApiData.requestURL);
//		return Constants.KEYWORD_FAIL + " - Failure response | Request URL = " + restApiData.requestURL;
//	}
//
//	// Assumes that the URL template variables have been fulfilled
//	public String sendPostURLtemplate() {
//		if (restApiData.requestURL.isEmpty()) {
//			restApiData.extentTest.log(LogStatus.FAIL, "Content-Type / Request Headers / Request Body / URL not defined");
//			return Constants.KEYWORD_FAIL + " - Content-Type / Request Headers / Request Body / URL not defined";
//		}
//
//		if (!restApiData.requestContentType.isEmpty() && restApiData.requestHeaders.isEmpty() && restApiData.requestBody.isEmpty())
//			restApiData.postResponsePayload = given().contentType(restApiData.requestContentType).post(restApiData.requestURL);
//		else if (!restApiData.requestContentType.isEmpty() && !restApiData.requestHeaders.isEmpty() && restApiData.requestBody.isEmpty())
//			restApiData.postResponsePayload = given().contentType(restApiData.requestContentType).headers(restApiData.requestHeaders).post(restApiData.requestURL);
//		else if (!restApiData.requestContentType.isEmpty() && !restApiData.requestHeaders.isEmpty() && !restApiData.requestBody.isEmpty())
//			restApiData.postResponsePayload = given().contentType(restApiData.requestContentType).headers(restApiData.requestHeaders).body(restApiData.requestBody).post(restApiData.requestURL);
//		else {
//			restApiData.extentTest.log(LogStatus.FAIL, "Content-Type / Request Headers / Request Body / URL not defined");
//			return Constants.KEYWORD_FAIL + " - Content-Type / Request Headers / Request Body / URL not defined";
//		}
//
//		if (restApiData.postResponsePayload.getBody().asString().contains("SUCCESS")) {
//			jsonFormat.storedJsonPayload = restApiData.postResponsePayload.getBody().asString();
//			restApiData.extentTest.log(LogStatus.PASS, "Successful response | Request URL = " + restApiData.requestURL);
//			return Constants.KEYWORD_PASS + " - Successful response | Request URL = " + restApiData.requestURL;
//		}
//		restApiData.extentTest.log(LogStatus.FAIL, "Failure response | Request URL = " + restApiData.requestURL);
//		return Constants.KEYWORD_FAIL + " - Failure response | Request URL = " + restApiData.requestURL;
//	}
//
//	public String sendGetURLwithAuth() {
//		if(restApiData.DataObjects[0].isEmpty() || restApiData.requestContentType.isEmpty()) {
//			restApiData.extentTest.log(LogStatus.FAIL, "Both URL and Content-Type needs to be defined");
//			return Constants.KEYWORD_FAIL + " - Both URL and Content-Type needs to be defined";
//		}
//
//		restApiData.requestURL = restApiData.DataObjects[0];
//		if ( restApiData.requestHeaders.get("X-Agencyport-Token") == null ||
//			 restApiData.requestHeaders.get("X-Agencyport-CSRF-Token") == null ) {
//			restApiData.extentTest.log(LogStatus.FAIL, "X-Agencyport-Token AND X-Agencyport-CSRF-Token in header is not defined.");
//			return Constants.KEYWORD_FAIL + " - X-Agencyport-Token AND X-Agencyport-CSRF-Token in header is not defined.";
//		}
//
//		restApiData.getResponsePayload = given().contentType(restApiData.requestContentType).headers(restApiData.requestHeaders).get(restApiData.requestURL);
//		if (restApiData.getResponsePayload.getBody().asString().contains("SUCCESS")) {
//			jsonFormat.storedJsonPayload = restApiData.getResponsePayload.getBody().asString();
//			restApiData.extentTest.log(LogStatus.PASS, "GET Request URL = " + restApiData.requestURL);
//			return Constants.KEYWORD_PASS + " - Successful response | Request URL = " + restApiData.requestURL;
//		}
//		restApiData.extentTest.log(LogStatus.FAIL, "GET Request URL = " + restApiData.requestURL);
//		return Constants.KEYWORD_FAIL + " - Failure response | Request URL = " + restApiData.requestURL;
//	}
//
//	public String sendGetURL() {
//		// Optionally can change the Request URL
//		if (restApiData.DataObjects[0].isEmpty() && restApiData.requestURL.isEmpty()) {
//			restApiData.extentTest.log(LogStatus.FAIL, "Send GET URL string is empty");
//			return Constants.KEYWORD_FAIL + " - Send GET URL string is empty";
//		}
//		else if (!restApiData.DataObjects[0].isEmpty())
//			restApiData.requestURL = restApiData.DataObjects[0];
//		if (restApiData.requestContentType != null && !restApiData.requestContentType.isEmpty()) {
//			if (restApiData.requestHeaders != null && !restApiData.requestHeaders.isEmpty())
//				restApiData.getResponsePayload = given().contentType(restApiData.requestContentType).headers(restApiData.requestHeaders).get(restApiData.requestURL);
//			else
//				restApiData.getResponsePayload = given().contentType(restApiData.requestContentType).get(restApiData.requestURL);
//		}
//		else {
//			restApiData.extentTest.log(LogStatus.FAIL, "Request Content-Type string is emtpy");
//			return Constants.KEYWORD_FAIL + " - Request Content-Type string is emtpy";
//		}
//
//		if (restApiData.getResponsePayload.getBody().asString().contains("SUCCESS")) {
//			jsonFormat.storedJsonPayload = restApiData.getResponsePayload.getBody().asString();
//			restApiData.extentTest.log(LogStatus.PASS, "GET Request URL = " + restApiData.requestURL);
//			return Constants.KEYWORD_PASS + " - Successful response | Request URL = " + restApiData.requestURL;
//		}
//		restApiData.extentTest.log(LogStatus.FAIL, "GET Request URL = " + restApiData.requestURL);
//		return Constants.KEYWORD_FAIL + " - Failure response | Request URL = " + restApiData.requestURL;
//	}
//	
//	public String setRequestURL() {
//		if (restApiData.DataObjects[0].isEmpty()) {
//			restApiData.extentTest.log(LogStatus.FAIL, "SetRequestURL DATA is Empty");
//			return Constants.KEYWORD_FAIL + " - SetRequestURL DATA is Empty";
//		}
//		restApiData.requestURL = restApiData.DataObjects[0];
//		restApiData.extentTest.log(LogStatus.PASS, "Request URL = " + restApiData.requestURL);
//		return Constants.KEYWORD_PASS + " - Request URL = " + restApiData.requestURL;
//	}
//
//	
//	public String sendPostURLwithGetResponseBody() {
//		if (restApiData.requestURL.isEmpty() && restApiData.DataObjects[0].isEmpty()) {
//			restApiData.extentTest.log(LogStatus.FAIL, "Send GET URL string and objectpayload cannot be empty");
//			return Constants.KEYWORD_FAIL + " - Send GET URL string and objectpayload cannot be empty";
//		}
//		else if (!restApiData.DataObjects[0].isEmpty())	// assuming that requestURL has been defined
//			restApiData.requestURL = restApiData.DataObjects[0];
//
//		if (restApiData.requestContentType != null && !restApiData.requestContentType.isEmpty()) {
//			if (restApiData.requestHeaders != null && !restApiData.requestHeaders.isEmpty()) {
//				if (restApiData.getResponsePayload != null) {
//					int subStringIndex = restApiData.getResponsePayload.asString().indexOf(restApiData.DataObjects[1]);
//					if (subStringIndex == -1) {
//						restApiData.extentTest.log(LogStatus.FAIL, "[" + restApiData.DataObjects[1] + "] not found inside JSON response payload");
//						return Constants.KEYWORD_FAIL + " - [" + restApiData.DataObjects[1] + "] not found inside JSON response payload";
//					}
//
//					String payload = jsonFormat.getJsonFromKeyAndPayload(restApiData.DataObjects[1], restApiData.getResponsePayload.asString());				
//					restApiData.postResponsePayload = given().contentType(restApiData.requestContentType).headers(restApiData.requestHeaders).body(payload).post(restApiData.requestURL);
//					if (restApiData.postResponsePayload.getBody().asString().contains("SUCCESS")) {
//						jsonFormat.storedJsonPayload = restApiData.postResponsePayload.getBody().asString();
//						restApiData.extentTest.log(LogStatus.PASS, "Successful response | Request URL = " + restApiData.requestURL);
//						return Constants.KEYWORD_PASS + " - Successful response | Request URL = " + restApiData.requestURL;
//					}
//				}
//			}
//		}
//		restApiData.extentTest.log(LogStatus.FAIL, "Failure response from using URL = " +restApiData.requestURL);
//		return Constants.KEYWORD_FAIL + " - Failure response from using URL = " +restApiData.requestURL;
//	}
//
//	
//	public String clearAllRestApiData() {
//		restApiData.clearAll();
//		restApiData.extentTest.log(LogStatus.PASS, "Cleared all REST API Variables and Templates");
//		return Constants.KEYWORD_PASS + " - Cleared all REST API Variables and Templates";
//	}
//
//
//	public String verifyJsonTrue() {
//		String storedValue = jsonFormat.getJsonFromKeyAndStoredPayload(restApiData.DataObjects[0]);
//		if (storedValue.equals(restApiData.DataObjects[1])) {
//			restApiData.extentTest.log(LogStatus.PASS, restApiData.DataObjects[0] + " was found inside the Json Payload");
//			return Constants.KEYWORD_PASS + " - " + restApiData.DataObjects[0] + " | value = " + restApiData.DataObjects[1] + " is equal to " + storedValue;
//		}
//		restApiData.extentTest.log(LogStatus.FAIL, restApiData.DataObjects[0] + " was not found inside the Json Payload");
//		return Constants.KEYWORD_FAIL + " - " + restApiData.DataObjects[0] + " | value = " + restApiData.DataObjects[1] + " is NOT equal to " + storedValue;
//	}
//
//	public String verifyJsonFalse() {
//		String storedValue = jsonFormat.getJsonFromKeyAndStoredPayload(restApiData.DataObjects[0]);
//		if (!storedValue.equals(restApiData.DataObjects[1])) {
//			restApiData.extentTest.log(LogStatus.PASS, restApiData.DataObjects[0] + " was not found inside the Json Payload");
//			return Constants.KEYWORD_PASS + " - " + restApiData.DataObjects[0] + " | value = " + restApiData.DataObjects[1] + " is NOT equal to " + storedValue;
//		}
//		restApiData.extentTest.log(LogStatus.FAIL, restApiData.DataObjects[0] + " was found inside the Json Payload");
//		return Constants.KEYWORD_FAIL + " - " + restApiData.DataObjects[0] + " | value = " + restApiData.DataObjects[1] + " is equal to " + storedValue;
//	}
//	
//	public String verifyHeaderTrue() {
//		String objectStr = restApiData.DataObjects[0];
//		if (restApiData.requestHeaders.containsKey(objectStr)) {
//			if (!restApiData.requestHeaders.get(objectStr).isEmpty())  {
//				restApiData.extentTest.log(LogStatus.PASS, objectStr + " value is populated");
//				return Constants.KEYWORD_PASS + " - " + objectStr + " value is populated";
//			}
//		}
//		else if (restApiData.requestHeaders.containsValue(objectStr)) {
//			restApiData.extentTest.log(LogStatus.PASS, objectStr + " key is populated");
//			return Constants.KEYWORD_PASS + " - " + objectStr + " key is populated";
//		}
//		restApiData.extentTest.log(LogStatus.FAIL, objectStr + " key/value was not populated");
//		return Constants.KEYWORD_FAIL + " - " + objectStr + " key/value was not populated";
//	}
//	
//	// Find the key(s) inside the saved JSON Response payload, and set the value(s)
//	public String setJsonKeyIntoResponseBody() {
//		int objLength = restApiData.DataObjects.length;
//		String key = "";
//		String value = "";
//		if (jsonFormat.storedJsonPayload.isEmpty()) {
//			restApiData.extentTest.log(LogStatus.FAIL, "the Response Body is empty, therefore cannot set key-value pair");
//			return Constants.KEYWORD_FAIL + " - the Response Body is empty, therefore cannot set key-value pair";
//		}
//		for (int i = 0; i < objLength && objLength > 2; i+=2) {
//			key = restApiData.DataObjects[i];
//			value = restApiData.DataObjects[i+1];
//			if(!jsonFormat.setJsonValueToStoredPayload(key, value)) {
//				restApiData.extentTest.log(LogStatus.FAIL, key+ "[" +value+ "]" + " is not found in the stored payload");
//				return Constants.KEYWORD_FAIL + " - " +key+ "[" +value+ "]" + " is not found in the stored payload";
//			}
//		}
//		restApiData.extentTest.log(LogStatus.PASS, "keys-value pair(s) inserted into Response Body");
//		return Constants.KEYWORD_PASS + " - keys-value pair(s) inserted into Response Body";
//	}
//	
//	// Copy the Stored Response JSON Payload into the Request body 
//	public String setRequestBodyFromResponsePayload() {
//		restApiData.requestBody = jsonFormat.storedJsonPayload;
//		if (restApiData.requestBody.isEmpty()) {
//			restApiData.extentTest.log(LogStatus.FAIL, "Payload is empty, therefore Request Body is empty");
//			return Constants.KEYWORD_FAIL + " - Nothing stored inside the Response Payload. Nothing to copy to Request Body.";
//		}
//		restApiData.extentTest.log(LogStatus.PASS, "Response Payload was copied into the Request Body");
//		return Constants.KEYWORD_PASS + " - Response Payload was copied into the Request Body";
//	}
}
